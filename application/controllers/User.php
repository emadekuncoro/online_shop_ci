<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{
			$d['judul'] = 'Halaman Member | ';
			$this->load->view('web/header', $d);
			$this->load->view('web/user');
			$this->load->view('web/footer');
		}
		else
		{
			redirect('user/daftar');
		}
	}
	public function daftar()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{
			redirect('user');
		}
		$d['judul'] = 'Login atau Daftar | ';
		$this->load->view('web/header', $d);
		$this->load->view('web/login');
		$this->load->view('web/footer');
	}
	public function validateLogin()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA")
		{
			redirect('user');
		}
		else
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if($this->form_validation->run() == FALSE)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	public function login()
	{
		if(!$this->input->is_ajax_request())
		{
			redirect('user');
		}
		if($this->session->userdata('isLoggedIn') == "userTOA")
		{
			redirect('user');
		}
		else
		{
			// cek validasi
			$result = $this->validateLogin();
			if(!$result)
			{
				$msg = array('success' => false,'isi' => validation_errors());
			}
			else
			{
				$data = array(
					"email" => $this->input->post('email', TRUE),
					"password" => $this->input->post('password', TRUE),
					);
				// cek apakah data login ditemukan
				$cek = $this->Webmodel->cekLogin($data);
				if(!$cek)
				{
					// jika tidak ditemukan maka login gagal
					$msg = array('success' => false,'isi' => "Username atau Password yang anda masukan salah, Silakan Ulangi..");
				} 
				else
				{
					// jika return true maka login berhasil 
					// cek apakah user aktif
					$data2 = array(
						"email" => $this->input->post('email', TRUE),
						"password" => $this->input->post('password', TRUE),
						"status" => 1
						);
					$cek_status = $this->Webmodel->cekUserStatus($data2);
					if(!$cek_status)
					{
						$msg = array('success'=>false, 'isi'=>"Akun anda belum di aktifkan, Silakan cek email untuk mengaktifkan akun anda.");
					}
					else
					{
						$msg = array('success' => true,'isi' => "Login sukses.. Anda akan dialihkan ke halaman Member..");
					}
				}
			}
			echo json_encode($msg);
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('user');
	}
	public function validateRegister()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA")
		{
			redirect('user');
		}
		else
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required|integer');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
			if($this->form_validation->run() == FALSE)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	public function register()
	{
		if(!$this->input->is_ajax_request())
		{
			redirect('user');
		}
		if($this->session->userdata('isLoggedIn') == "userTOA")
		{
			redirect('user');
		}
		else
		{
			// cek validasi
			$result = $this->validateRegister();
			if(!$result)
			{
				$msg = array('success' => false,'isi' => validation_errors());
			}
			else
			{
				$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
				$options = [
	    		'cost' => 12,
	    		'salt' => $salt,
				];
				$pass = password_hash($this->input->post('password', TRUE), PASSWORD_BCRYPT, $options);
				$data = array(
					"kode_member" => $this->Webmodel->ambilKode('kode_member','toa_member','MTO_'),
					"email" => $this->input->post('email', TRUE),
					"no_telp" => $this->input->post('no_telp', TRUE),
					"password" => $pass,
					"status" => 0,
					"key" => str_replace(array('~','/','!','@','#','$','%','^','&','*','(',')','>','<','.',',',';',':'), '', password_hash(date('YmdHis').rand(111,999), PASSWORD_DEFAULT))
					);
				$link = base_url('user/activateAccount/'.$data['key']);
				// cek apakah data login ditemukan
				$cek = $this->Webmodel->cekDuplikat($data['email']);
				if(!$cek)
				{
					// jika tidak ditemukan maka login gagal
					$msg = array('success' => false,'isi' => "Email yang anda masukan sudah terdaftar, silakan login untuk melanjutkan.");
				} 
				else
				{
					$daftar = $this->Webmodel->regUser($data);
					if(!$daftar)
					{
						$msg = array('success'=>false, 'isi'=>"Proses pendaftaran Gagal, Silakan Ulangi.");
					}
					else
					{
						$message = 	"
								<div style='box-shadow:0 0 5px #FE980F;background:#FE980F;color:#FFF;border:2px solid #F9F9F9;border-radius:5px;padding:10px;font-size:20px;font-family:simplex;'>
								<b>Konfirmasi Registrasi</b>
								<div style='background:#FCF8E3;color:#FE980F;border:2px solid #F9F9F9;border-radius:5px;padding:10px;margin:10px;'>
								<b>Terima kasih telah melakukan registrasi. <br>
								Silakan klik link dibawah ini untuk menyelesaikan proses registrasi.</b> <br><br>
								<a href='".$link."' style='text-decoration:none;border:2px solid #fff; color:#fff; background:#FE980F;padding:5px;border-radius:5px;'><b>Link Aktifasi</b></a>
								<br>
								<br>
								</div>
								</div>
									";
						
						$this->load->library('email');
						$this->email->from('ehaku@momandkidsgoods.com','Admin MomAndKidsGoods[dot]com');
						$this->email->to($data['email']); 
						$this->email->subject('Konfirmasi Registrasi');
						$this->email->message($message);
						//ubah dibawah ini
						
						if($this->email->send())
						{
							$msg = array('success' => true,'isi' => "
							 1. Proses Registrasi berhasil, Silakan cek email untuk mengaktifkan akun anda. <br>
							 2. Periksa inbox (kotak masuk) email anda, jika tidak ada cek juga folder spam. <br>
							 3. Jika email kami masuk folder spam, silakan tandai email kami sebagai 'Bukan Spam'  ");
						}
						else
						{
							$msg = array('success'=>false, 'isi'=> $this->email->print_debugger());
						}
					}
				}
			}
			echo json_encode($msg);
		}
	}
	public function activateAccount($kode)
	{
		if($this->session->userdata('isLoggedIn') == "userTOA")
		{
			redirect('user');
		}
		else
		{
			$cek = $this->Webmodel->cekUserKey($kode);
			if(!$cek)
			{
				redirect('web');
			}
			else
			{
				$action = $this->Webmodel->activateKey($kode);
				if(!$action)
				{
					$this->session->set_flashdata('result', 'Proses Aktivasi Akun Gagal, Silakan login jika anda sudah pernah mengaktifkan akun anda.');
				}
				else
				{
					$this->session->set_flashdata('result', 'Proses Aktivasi Akun Berhasil, Silakan login untuk masuk ke halaman member.');
				}
				redirect('user/daftar');
			}
		}
	}
	public function ResetPass()
	{
		if(!$this->input->is_ajax_request())
		{
			redirect('user');
		}
		if($this->session->userdata('isLoggedIn') == "userTOA")
		{
			redirect('user');
		}
		else
		{
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			if($this->form_validation->run() == FALSE)
			{
				$msg = array('success' => false,'isi' => validation_errors());
			}
			else
			{
				$data = array( "email" => $this->input->post('email', TRUE), );
				// cek apakah data login ditemukan
				$cek = $this->Webmodel->cekDuplikat($data['email']);
				if($cek)
				{
					$msg = array('success' => false,'isi' => "Email yang anda masukan belum terdaftar, silakan mendaftar untuk melanjutkan.");
				} 
				else
				{
					$resetPW = $this->Webmodel->resetPW($data);
					if(!$resetPW)
					{
						$msg = array('success'=>false, 'isi'=>"Proses Reset Password Gagal, Silakan Ulangi.");
					}
					else
					{
						$link = base_url('user');
						$message = 	"
								<div style='box-shadow:0 0 5px #FE980F;background:#FE980F;color:#FFF;border:2px solid #F9F9F9;border-radius:5px;padding:10px;font-size:20px;font-family:simplex;'>
								<b>Berhasil Reset Password</b>
								<div style='background:#FCF8E3;color:#FE980F;border:2px solid #F9F9F9;border-radius:5px;padding:10px;margin:10px;'>
								<b>Anda telah berhasil melakukan proses reset password. <br>
								Silakan login dengan password ".$resetPW." </b> <br><br>
								<a href='".$link."' style='text-decoration:none;border:2px solid #fff; color:#fff; background:#FE980F;padding:5px;border-radius:5px;'><b>Login</b></a>
								<br>
								<br>
								</div>
								</div>
									";
						$this->load->library('email');
						$this->email->set_newline("\r\n");
						$this->email->from('webmaster@bikinaplikasiweb.com','Admin Online Shop');
						$this->email->to($data['email']); 
						$this->email->subject('Reset Password Toko Online');
						$this->email->message($message);
						$res = $this->email->send();
						// var_dump($res);exit;
						if($this->email->send())
						{
							$msg = array('success' => true,'isi' => "
								1. Proses Reset Password berhasil, Silakan cek email untuk melihat password baru anda. <br>
								2. Periksa inbox (kotak masuk) email anda, jika tidak ada cek juga folder spam.  <br>
								3. Jika email kami masuk folder spam, silakan tandai email kami sebagai 'Bukan Spam' ");
						}
						else
						{
							$msg = array('success'=>false, 'isi'=>"Proses Reset Password Gagal, Email Gagal dikirim.");
						}
					}
				}
			}
			echo json_encode($msg);
		}
	}

	public function pengaturan()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{
			$d['judul'] = 'Pengaturan Profil | ';
			$this->load->view('web/header', $d);
			$this->load->view('web/set_user');
			$this->load->view('web/footer');
		}
		else
		{
			redirect('user/daftar');
		}
	}

	public function ubah_profil()
	{
		if($this->session->userdata('isLoggedIn') != "userTOA") 
		{
			redirect('user/daftar');
		}
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('pass_lama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('pass_baru', 'Password Baru', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('ulang_pass', 'Ulangi Password Baru', 'trim|required|matches[pass_baru]');
		
		if($this->form_validation->run() == FALSE )
		{
			$this->pengaturan();
		}
		else
		{

			$kode = $this->input->post('kode', TRUE);
			$pass_lama = $this->input->post('pass_lama', TRUE);
			$cek = $this->Webmodel->cekPassLama($pass_lama, $kode);

			if(!$cek)
			{
				$this->session->set_flashdata('result', 'Password yang anda masukan salah, silakan ulangi..');
				$this->pengaturan();
			}
			else
			{
				$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
				$options = [
	    		'cost' => 12,
	    		'salt' => $salt,
				];
				$data['password'] = password_hash($this->input->post('pass_baru', TRUE), PASSWORD_BCRYPT, $options);

				$this->Webmodel->update_data('toa_member', $data, array('kode_member'=>$kode));
				redirect('user/logout');
			}
		}

	}

	public function konfirmasi_pembayaran()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{
			// $data['konfirmasi'] = $this->db->get_where('toa_order', array('kode_member'=>$this->session->userdata('kode'), 'status'=>'Belum Bayar'));
			$data['konfirmasi'] = $this->db->get_where('toa_order', array('kode_member'=>$this->session->userdata('kode'), 'status'=>'Belum Bayar'));
			$data['judul'] = 'Konfirmasi Pembayaran | ';
			$this->load->view('web/header', $data);
			$this->load->view('web/konfirmasi_pembayaran', $data);
			$this->load->view('web/footer');
		}
		else
		{
			redirect('user/daftar');
		}
	}



	public function detail_pembayaran($id)
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{
			$data['detail1'] = $this->db->get_where('toa_order', array('kode_order'=>$id));
			$data['detail2'] = $this->db->get_where('toa_detail_order', array('kode_order'=>$id));
			$data['judul'] = 'Detail Pembayaran | ';
			$this->load->view('web/header', $data);
			$this->load->view('web/detail_pembayaran', $data);
			$this->load->view('web/footer');
		}
		else
		{
			redirect('user/daftar');
		}
	}

	public function input_pembayaran($id)
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{
			$data['kode'] = $id;
 			$data['bank'] = $this->db->get('toa_bank');
 			$data['judul'] = 'Masukan Detail Pembayaran | ';
			$this->load->view('web/header', $data);
			$this->load->view('web/input_pembayaran', $data);
			$this->load->view('web/footer');
		}
		else
		{
			redirect('user/daftar');
		}
	}

	public function kirimPembayaran()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{

			$data['kode_order'] = $this->input->post('kode', TRUE);
			
			$this->form_validation->set_rules('tgl_bayar', 'Tanggal Pembayaran', 'trim|required');
			$this->form_validation->set_rules('jml_bayar', 'Jumlah Pembayaran', 'trim|required');
			$this->form_validation->set_rules('no_rek', 'No Rekening', 'trim|required');
			$this->form_validation->set_rules('atas_nama', 'Nama Rekening', 'trim|required');
			$this->form_validation->set_rules('bank_tujuan', 'Nama Bank Tujuan', 'trim|required');
			if($this->form_validation->run() == false)
			{
				$this->input_pembayaran($data['kode_order']);
			}
			else
			{
				$cek = $this->db->get_where('toa_pembayaran', array('kode_order'=>$data['kode_order']));
				if($cek->num_rows() == 1)
				{
					$this->session->set_flashdata('result', 'Anda telah melakukan Konfirmasi Pembayaran pada pesanan ini, silakan tunggun email pemberitahuan dari kami. <br> Setelah kami memverifikasi data anda, kami akan mengirimkan email pemberitahuan yang berisi resi pengiriman pesanan anda.');
					$this->konfirmasi_pembayaran();
				}
				else
				{
					$data['kode_pembayaran'] = md5(date('YmdHis').'!@#$toa.Konfirmasi93'.rand(100000,999999));
					$data['tgl_bayar'] = $this->input->post('tgl_bayar', TRUE);
					$data['jml_bayar'] = $this->input->post('jml_bayar', TRUE);
					$data['no_rek'] = $this->input->post('no_rek', TRUE);
					$data['atas_nama'] = $this->input->post('atas_nama', TRUE);
					$data['bank_tujuan'] = $this->input->post('bank_tujuan', TRUE);
					$data['status'] = 'PENDING';

					$this->Webmodel->kirim_data_konfirmasi('toa_pembayaran', $data);
					$this->db->update('toa_order', array('status'=>'Proses'), array('kode_order'=>$data['kode_order']));
					$this->input_pembayaran($data['kode_order']);
					echo "<meta http-equiv='refresh' content='10; url=".base_url()."user'>";

				}

			}
		}
		else
		{
			redirect('user/daftar');
		}
	}

	public function riwayat_transaksi()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{
			$data['judul'] = 'Semua Transaksi | ';
			$data['order'] = $this->db->get_where('toa_order', array('kode_member'=>$this->session->userdata('kode')));
			$this->load->view('web/header', $data);
			$this->load->view('web/riwayat_transaksi', $data);
			$this->load->view('web/footer');
		}
		else
		{
			redirect('user/daftar');
		}
	}

	public function ubah_alamat($id)
	{
		if($this->session->userdata('isLoggedIn') == "userTOA") 
		{
			$order = $this->db->get_where('toa_order', array('kode_order'=>$id));
			foreach ($order->result() as $value) {
				$data['nama_penerima'] = $value->nama_penerima;
				$data['alamat1'] = $value->alamat1;
				$data['alamat2'] = $value->alamat2;
			}
			$data['kode'] = $id;
			$data['lokasi'] = $this->Webmodel->lihatLokasi();

			$data['judul'] = 'Ubal Alamat Pengiriman | ';

			$this->load->view('web/header', $data);
			$this->load->view('web/ubah_alamat', $data);
			$this->load->view('web/footer');
		}
		else
		{
			redirect('user/daftar');
		}
	}

	public function update_alamat()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA")
		{
			$this->form_validation->set_rules('nama_penerima', 'Nama Penerima', 'trim|required');
			$this->form_validation->set_rules('alamat1', 'Alamat Lengkap', 'trim|required');
			$this->form_validation->set_rules('kelurahan', 'Kelurahan', 'trim|required');
			$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required');
			$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required');
			$this->form_validation->set_rules('kota', 'Kota', 'trim|required');
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required');
			$this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim|required|integer');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required|integer');

			if($this->form_validation->run()==FALSE)
			{
				$msg = array('stat'=>false, 'isi'=>validation_errors());
				
			}
			else
			{

				$data['kode_order'] = $this->input->post('kode_order', TRUE);
				$data['nama_penerima'] = $this->input->post('nama_penerima', TRUE);
				$data['alamat1'] = $this->input->post('alamat1', TRUE);
				$data['alamat2']  = $this->input->post('kelurahan', TRUE).'<br>Kode Pos: '. $this->input->post('kode_pos', TRUE) . '<br> No. Telp : '. $this->input->post('no_telp', TRUE); 
				$data['ongkir'] = 0;
				$data['total_bayar'] = 0;
				$data['status'] = 'Pending';

				$cek = $this->Webmodel->update_alamat($data);
				if(!$cek)
				{
					$msg = array('stat'=>false, 'isi'=>'Gagal meng-update alamat, silakan coba lagi..');
				}
				else
				{
					$msg = array('stat'=>true, 'isi'=>'Berhasil meng-update alamat, Anda akan dialihkan ke halaman member..');

				}
			}

			echo json_encode($msg);
			
		}
		else
		{
			redirect('user/daftar');
		}
	}


}
/* End of file User.php */
/* Location: ./application/controllers/User.php */