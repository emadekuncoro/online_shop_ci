<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keranjang extends CI_Controller {


	public function index()
	{
		$d['judul'] = 'Keranjang Belanja | ';
		$d['lokasi'] = $this->Webmodel->lihatLokasi();
		$d['bank'] = $this->db->get('toa_bank');
		$d['kurir'] = $this->db->get('toa_kurir');

		$this->load->view('web/header',$d);
		$this->load->view('web/keranjang',$d);
		$this->load->view('web/footer');
	}

	public function tambah_barang()
	{
		$data = array(
			'id'      => $this->input->post('id'),
			'qty'     => $this->input->post('qty'),
		    'price'	  => $this->input->post('price'),
		    'pic'	  => $this->input->post('pic'),
		    'berat'	  => $this->input->post('berat'),
			'name'    => $this->input->post('name'));
		$this->cart->insert($data);
		// redirect('keranjang');
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."keranjang'>";

	}
	
	
	public function update_keranjang()
	{
		$total = count($this->cart->contents());
		$item = $this->input->post('rowid[]');
		$qty = $this->input->post('qty[]');
		for($i=0;$i < $total;$i++)
		{
			$data = array(
			'rowid' => $item[$i],
			'qty'   => $qty[$i]);
			$q = $this->cart->update($data);
		}
	
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."keranjang'>";

		// redirect('keranjang');
	}

	public function hapus_item($kode)
	{
		$id = $this->uri->segment(3) ? $this->uri->segment(3):'';		
	
		$data = array(
			'rowid' => $kode,
			'qty'   => 0);
			$this->cart->update($data);
		redirect('keranjang');
	}

	public function cekLogin()
	{
		if($this->session->userdata('isLoggedIn') == "userTOA")
		{
			$this->index();
		}
		else
		{
			redirect('user');
		}
	}

	public function data_kota_ajax()
	{
		$id = $this->input->get('id', TRUE);
		if($id!="")
		{
			$q = $this->Webmodel->lihatKota($id);
			echo '<option value="">Pilih Kabupaten/Kota</option>';
			foreach ($q->result_array() as $key => $value) 
			{ 
				echo '<option value="' .$value['lokasi_kabupatenkota'].  '">' .$value['lokasi_nama']. '</option>';
			}
		}
	}

	public function data_kec_ajax()
	{
		$prov = $this->input->get('prov', TRUE);
		$kota = $this->input->get('kota', TRUE);
		if($kota!="")
		{
			$q = $this->Webmodel->lihatKecamatan($prov, $kota);
			echo '<option value="">Pilih Kecamatan</option>';
			foreach ($q->result_array() as $key => $value) 
			{ 
				echo '<option value="' .$value['lokasi_kecamatan'].  '">' .$value['lokasi_nama']. '</option>';
			}
		}
	
	}

	public function data_kel_ajax()
	{
		$prov = $this->input->get('prov', TRUE);
		$kota = $this->input->get('kota', TRUE);
		$desa = $this->input->get('desa', TRUE);

		$nama_prov = $this->input->get('nama_prov', TRUE);
		$nama_kota = $this->input->get('nama_kota', TRUE);
		$nama_desa = $this->input->get('nama_desa', TRUE);
		$alamat2 = 'Provinsi :'.$nama_prov.'<br> Kabupaten/Kota :'.$nama_kota.'<br> Kecamatan :'.$nama_desa.'<br> Kelurahan/Desa :';

		if($desa!="")
		{
			$q = $this->Webmodel->lihatKelurahan($prov, $kota, $desa);
			echo '<option value="">Pilih Kelurahan/Desa</option>';
			foreach ($q->result_array() as $key => $value) 
			{ 
				echo '<option value="' .$alamat2.$value['lokasi_nama'].  '">' .$value['lokasi_nama']. '</option>';
			}
		}
	
	}
}

/* End of file Keranjang.php */
/* Location: ./application/controllers/keranjang.php */
