<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sendmail extends CI_Controller {

	public function index()
	{
		$this->load->library('email');

		$this->email->from('ucihamadera@gmail.com', 'Your Name');
		$this->email->to('arumdiyahp@gmail.com');

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');

		echo $result = ($this->email->send()) ? 'sukses': $this->email->print_debugger(); 
	}

}

/* End of file Sendmail.php */
/* Location: ./application/controllers/Sendmail.php */