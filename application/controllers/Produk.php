<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {	
	
	public function index()
	{
		redirect('produk/cari');
	}

	public function cari()
	{
		$d['judul'] = 'Semua Produk | ';
		
		if($this->input->post("cari")=="")
			{
				$kata = $this->session->userdata('kata');
			}
			else
			{
				$sess_data['kata'] = $this->input->post("cari");
				$this->session->set_userdata($sess_data);
				$kata = $this->session->userdata('kata');
			}

		$page=$this->uri->segment(3);
		$limit=6;
		if(!$page):
			$offset = 0;
		else:
			$offset = $page;
		endif;
		
		$d["produk"] = ($this->input->post("cari")=="") ? $this->Adminmodel->lihat_tabel('toa_produk','tgl_insert','DESC',$limit,$offset):$this->Adminmodel->lihat_tabel_cari('toa_produk','tgl_insert', 'DESC', $limit, $offset, array('nama_produk'=>$kata));
		
		$config['base_url'] = base_url() . 'produk/cari';
		$config['total_rows'] = ($this->input->post("cari")=="") ? $this->Adminmodel->hitung_isi_tabel('toa_produk',''):$this->Adminmodel->hitung_isi_tabel_cari('toa_produk', 'nama_produk', $kata);

		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$d["paginator"] =$this->pagination->create_links();
		$d['tot'] = $offset; 
		$d['kategori'] = $this->db->get_where('toa_kategori', array('kode_level'=>0, 'kode_parent'=>0));


		$this->load->view('web/header',$d);
		$this->load->view('web/sidebar',$d);
		$this->load->view('web/produk',$d);
		$this->load->view('web/footer',$d);
	}

	public function pencarian()
	{
		$d['judul'] = 'Pencarian Produk | ';

		$page=$this->uri->segment(3);
		$limit=6;
		if(!$page):
			$offset = 0;
		else:
			$offset = $page;
		endif;	
		
		$id = $this->input->post('cari');
		
		$d["produk"] = $this->db->get_where('toa_produk', array('id_kategori'=>$id), $limit, $offset);
		$config['base_url'] = base_url() . 'produk/pencarian';
		$config['total_rows'] =	$this->db->get_where('toa_produk', array('id_kategori'=>$id))->num_rows();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$d["paginator"] =$this->pagination->create_links();
		$d['tot'] = $offset; 
		$d['kategori'] = $this->db->get_where('toa_kategori', array('kode_level'=>0, 'kode_parent'=>0));


		$this->load->view('web/header',$d);
		$this->load->view('web/sidebar',$d);
		$this->load->view('web/produk',$d);
		$this->load->view('web/footer',$d);
	}

	public function detail($link)
	{

		$kode = $this->uri->segment(3) ? $this->uri->segment(3):'';		
	
		$p_kode = explode("/",$kode);
		$d['detail_produk'] = $this->db->get_where('toa_produk', array('kode_produk'=>$p_kode[0]));
		$judul = "";
		$kd_kategori = "";
		foreach($d['detail_produk']->result() as $dp)
		{
			$d['kode'] = $dp->kode_produk;
			$d['harga'] = $dp->harga;
			$d['stok'] = $dp->stok;
			$d['berat'] = $dp->berat;
			$d['gambar'] = $dp->gambar;
			$d['nama_produk'] = $dp->nama_produk;
		}
		$d['judul'] = $d['nama_produk'].' | ';

		// $d['nama'] = 'Mom And Kids Goods | Detail Produk';
		// $d['deskripsi'] = 'Semua Produk Terbaru dari Toko Mom And Kids Goods';
		// $d['keywords'] = 'toko_online, toko_peralatan_bayi, toko peralatan_rumah_tangga,';
		$data = $this->db->get('toa_info');
		$d['kategori'] = $this->db->get_where('toa_kategori', array('kode_level'=>0, 'kode_parent'=>0));

		// foreach ($data->result() as $value) {
		// 	$d['alamat'] =$value->alamat;
		// 	$d['no_telp'] =$value->no_telp;
		// 	$d['email'] =$value->email;
		// 	$d['google'] =$value->google;
		// 	$d['fb'] =$value->fb;
		// 	$d['twitter'] =$value->twitter;
		// 	$d['logo'] =$value->logo;
		// }

		$d['produk_item'] = $this->Adminmodel->lihat_tabel('toa_produk', 'tgl_insert', 'DESC', 3,0);
		$d['produk_active'] = $this->Adminmodel->lihat_tabel('toa_produk', 'tgl_insert', 'DESC', 3,3);


		$this->load->view('web/header',$d);
		$this->load->view('web/sidebar',$d);
		$this->load->view('web/detail_produk',$d);
		$this->load->view('web/footer',$d);
		
	
	}


}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */