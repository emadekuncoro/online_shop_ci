<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function index()
	{
		if(!$this->input->is_ajax_request())
		{
			redirect('404');
		}

		if($this->session->userdata('isLoggedIn')!= "userTOA")
		{
			redirect('web');
		}

		if($this->validateChekout()==FALSE)
		{
			// $this->session->set_flashdata('result', validation_errors());
			// redirect('keranjang');
			$msg = array('stat' => false,'isi' => validation_errors());
		}
		else
		{
		
		
			//data order
			$data['kode_order'] = md5(date('YmdHis').'*&^%_cyberEhaku_!@#'.rand(111,999));
			$data['tgl_order'] = date('Y-m-d');
			// $kode_detail = $this->Webmodel->ambilKode('kode_detail', 'toa_detail_order', 'DTL_' );
			$data['kode_member'] = $this->input->post('kode_member', TRUE);
			$data['nama_penerima'] = $this->input->post('nama_penerima', TRUE);
			$data['alamat1'] = $this->input->post('alamat1', TRUE);
			$data['alamat2']  = $this->input->post('kelurahan', TRUE).'<br>Kode Pos: '. $this->input->post('kode_pos', TRUE) . '<br> No. Telp : '. $this->input->post('no_telp', TRUE); 
			$data['kode_bank'] = $this->input->post('bank', TRUE); 
			$data['kode_kurir'] =  $this->input->post('kurir', TRUE);	
			$data['sub_total'] = $this->cart->total();
			$data['ongkir'] = 0;
			$data['total_bayar'] = 0;
			$data['status'] = 'Pending';

			$simpanOrder = $this->Webmodel->simpanOrder($data);
			if(!$simpanOrder)
			{
				// $this->session->set_flashdata('result', 'Gagal simpan data order');
				// redirect('keranjang');
				$msg = array('stat'=>false, 'isi'=> 'Data Gagal disimpan, Silakan ulangi..');
			}
			else
			{
				// data detail order
				foreach($this->cart->contents() as $item)
				{
					$data = array( 
						'kode_produk'=>$item['id'],
						'nama_produk'=>$item['name'],
						'berat'=>$item['berat'],
						'harga'=>$item['price'],
						'jumlah'=>$item['qty'],
						'sub_berat'=>$item['berat']*$item['qty'],
						'kode_detail' => '',
						'kode_order' => $data['kode_order']

						);
					$simpan_detailOrder = $this->Webmodel->simpan_detailOrder($data);
				}
				if(!$simpan_detailOrder)
				{
					// $this->session->set_flashdata('result', 'Gagal simpan detail order');
					// redirect('keranjang');

					$msg = array('stat'=>false, 'isi'=> 'Data Detail Order Gagal disimpan, Silakan ulangi..');
				}
				else
				{

					$this->cart->destroy();
					// $this->session->set_flashdata('result', 'Data berhasil disimpan..');
					// redirect('keranjang');
					$msg = array('stat'=>true, 'isi'=> 'Pesanan anda telah berhasil dikirim, kami akan segera memproses pesanan anda dalam 1x24 jam.. 
						<br> Setelah pesanan anda kami konfirmasi anda akan menerima email pemberitahuan yang berisi detail pembayaran atas pesanan anda. 
						<br> Silakan tunggu selama 1x24 jam. Terima kasih..
						<br> Anda akan dialihkan ke halaman member..');
				}
			}

		}
			echo json_encode($msg);
		
	}


	public function validateChekout()
	{

		if($this->session->userdata('isLoggedIn')!= "userTOA")
		{
			redirect('404');
		}
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama_penerima', 'Nama Penerima', 'trim|required');
			$this->form_validation->set_rules('alamat1', 'Alamat Lengkap', 'trim|required');
			$this->form_validation->set_rules('bank', 'Bank', 'trim|required');
			$this->form_validation->set_rules('kurir', 'Kurir Pengiriman', 'trim|required');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required|integer');
			$this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim|required|integer');
			$this->form_validation->set_rules('kelurahan', 'Kelurahan/Desa', 'trim|required');
			return $this->form_validation->run();
			
	}

}

/* End of file CheckOut.php */
/* Location: ./application/controllers/CheckOut.php */