<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	

	public function index() 
	{
	    #Getting data from Database with Active Record
	    $this->db->order_by('tanggal', 'desc'); #order so newest posts would be first
	    $this->db->limit(15); #25 is a great number for RSS feeds 10-20 is too few
	    $query = $this->db->get('toa_artikel'); #make the query
	    #RSS data
	    $data['title'] = 'RSS Feed '.base_url(); #RSS feed title
	    $data['link'] = base_url(); #site link where the newest posts are, base_url() needs url helper
	    $data['description'] = 'Artikel Terbaru Dari Website Kami.'; #describe the feeds
	     
	    $data['items'] = $query->result(); #put posts to our $data array
	     
	    $this->_rss2($data); #generate RSS code
    }

    public function _rss2($data) 
    {
	    header('Content-type: text/xml');
	    echo '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">',
	    '<channel>',
	    '<title>',$data['title'],'</title>',
	    '<link>',$data['link'],'</link>',
	    '<description>',$data['description'],'</description>',
	    '<atom:link href="',base_url(),'feed" rel="self" type="application/rss+xml" />';
	    foreach($data['items'] as $item) echo
	    '<item>',
	    '<title>',xml_convert($item->judul),'</title>',
	    '<link>',base_url(),'artikel/detail/',$item->kode_artikel,'/',$item->tanggal,'/',str_replace(' ', '-', $item->judul),'.html','</link>',
	    '<description>',xml_convert($item->isi_artikel),'</description>',
	    '<pubDate>',date(DATE_RSS, strtotime($item->tanggal.$item->jam)),'</pubDate>',
	    '<guid>',base_url(),'artikel/detail/',$item->kode_artikel,'/',$item->tanggal,'/',str_replace(' ', '-', $item->judul),'.html','</guid>',
	    '</item>';
	    echo	'</channel>',
	    '</rss>';
    }

}

/* End of file Rss.php */
/* Location: ./application/controllers/Rss.php */