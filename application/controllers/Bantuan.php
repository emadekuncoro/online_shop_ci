<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bantuan extends CI_Controller {

	public function index()
	{
		$d['judul'] = 'Pusat Bantuan | ';
		$bantuan = $this->db->get('toa_info');
		foreach ($bantuan->result() as $value) {
			$data['cara_belanja'] = $value->cara_belanja;
			$data['kebijakan_privasi'] = $value->kebijakan_privasi;
			$data['retur_produk'] = $value->aturan_retur_produk;
			$data['tentang_kami'] = $value->tentang_kami;
		}
		$this->load->view('web/header', $d);
		$this->load->view('web/bantuan', $data);
		$this->load->view('web/footer');
	}

}

/* End of file Bantuan.php */
/* Location: ./application/controllers/Bantuan.php */