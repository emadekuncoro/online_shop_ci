<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>404 | Not Found</title>
    <link href="<?php echo base_url('asset'); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('asset'); ?>/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url('asset'); ?>/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url('asset'); ?>/css/responsive.css" rel="stylesheet">
</head><!--/head-->
<body>
	<div class="container text-center">
		<div class="logo-404">
			<a href="<?php echo base_url() ?>"><img src="<?php echo base_url('asset'); ?>/images/home/logo.png" alt="" /></a>
		</div>
		<div class="content-404">
			<img src="<?php echo base_url('asset') ?>/images/404/404.png" class="img-responsive" alt="" />
			<h1><b>OPPS!</b> Halaman Tidak Ditemukan</h1>
			<p>Halaman yang anda minta tidak ditemukan, silakan klik link di bawah ini untuk kembali ke halaman utama..</p>
			<h2><a href="<?php echo base_url(); ?>">Halaman Utama</a></h2>
		</div>
	</div>
</body>
</html>