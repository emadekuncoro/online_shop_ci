</div>
</div>
</section>
<footer id="footer"><!--Footer-->
	<div class="footer-widget">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="single-widget">
						<h2>Informasi</h2>
						<ul class="nav nav-pills nav-stacked">
							<li><a href="<?php echo base_url('bantuan'); ?>"><i class="fa fa-star"></i>Cara Belanja</a></li>
							<li><a href="<?php echo base_url('bantuan'); ?>"><i class="fa fa-star"></i>Layanan dan Kebijakan Privasi</a></li>
							<li><a href="<?php echo base_url('bantuan'); ?>"><i class="fa fa-star"></i>Retur Produk</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="single-widget">
						<h2>Selalu Terhubung</h2>
						<ul class="nav nav-pills nav-stacked">
							<li><a href="<?php $q=$this->db->get('toa_info'); $row = $q->row_array(); echo $fb = isset($row['fb'])?$row['fb']:''; ?>"><i class="fa fa-share"></i> Facebook</i></a></li>
							<li><a href="<?php echo $twitter = isset($row['twitter'])?$row['twitter']:''; ?>"><i class="fa fa-share"></i> Twitter</a></li>
							<li><a href="<?php echo $google = isset($row['google'])?$row['google']:''; ?>"><i class="fa fa-share"></i> Google</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="single-widget">
						<h2>RINGKASAN</h2>
						<ul class="nav nav-pills nav-stacked">
							<li><a href="<?php echo base_url('produk'); ?>"><i class="fa fa-pencil"></i>Produk Terbaru</a></li>
							<li><a href="<?php echo base_url('artikel'); ?>"><i class="fa fa-pencil"></i>Artikel Terbaru</a></li>
							<li><a href="<?php echo base_url('testimoni'); ?>"><i class="fa fa-pencil"></i>Testimoni</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<p class="pull-left">Copyright © 2015 | <a href="">Emade Haryo Kuncoro</a></p>
				<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span> Customed by <a href="">EHaKu</a></p>
			</div>
		</div>
	</div>
</footer><!--/Footer-->
</body>
</html>
