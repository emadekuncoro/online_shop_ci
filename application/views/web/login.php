<section id="form"><!--form-->
	<div class="container">
		<div class="row">
			<div class="login-message"></div>
			<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info text-center">'.$this->session->flashdata('result').'</div>':''; ?>
			<div class="col-sm-4 col-sm-offset-1">
				<h2 class="title text-center"> <i class="fa fa-user"></i></h2>
				<div class="login-form"><!--login form-->
					<div id="form-lupa">
						<h4>Password baru akan dikirim ke email anda.</h4>
						<?php echo form_open('user/resetPass', array('onsubmit'=>'return false','autocomplete'=>'off')); ?>
						<input required placeholder="Masukan Email" type="email" name="email">
						<button type="submit" id="btn-reset" class="btn btn-default">Reset Password <i class="fa fa-sign-in"></i> </button>
						<?php echo form_close(); ?>
					</div>
					<div id="form-login">
						<h2>Sialakan Masuk untuk melanjutkan</h2>
						<?php echo form_open('user/login', array('onsubmit'=>'return false','autocomplete'=>'off')); ?>
						<input required placeholder="Masukan Email" type="email" name="email">
						<input required placeholder="Masukan Password" type="password" name="password">
						<button id="btn-login" type="submit" class="btn btn-default">Login <i class="fa fa-sign-in"></i> </button>
						<button id="btn-lupa" class="btn pull-right"><i class="fa fa-lock"></i> Lupa Password</button>
						<?php echo form_close(); ?>
					</div>
				</div><!--/login form-->
			</div>
			<div class="col-sm-1">
				<h2 class="or">Atau</h2>
			</div>
			<div class="col-sm-4">
				<h2 class="title text-center"> <i class="fa fa-user"></i></h2>
				<div class="signup-form"><!--sign up form-->
					<h2>Belum terdaftar ? Daftar sekarang juga!</h2>
					<?php echo form_open('user/register', array('onsubmit'=>'return false','autocomplete'=>'off')); ?>
					<input required name="email"  placeholder="Masukan Email" type="email">
					<input required name="no_telp"  placeholder="Masukan No Telp : (085000000123) " type="text">
					<input required name="password" placeholder="Masukan Password" type="password">
					<button id="btn-signup" type="submit" class="btn btn-default">Daftar <i class="fa fa-sign-in"></i></button>
					<?php echo form_close(); ?>
				</div><!--/sign up form-->
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#form-lupa').hide();
	$('#btn-lupa').click(function(event) {
		$('#form-login').hide();
		$('#form-lupa').fadeIn(300);
	});
	$("#form-login form").submit(function() {
			// menampung data
			var base = '<?php echo base_url(); ?>';
			data = $("#form-login form").serialize();
			$("#btn-login").html('Memproses...');
			$.ajax({
				url: $(this).prop('action'),
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(msg) {
					if(msg.success==true) { //jika login berhasil maka muncul pesan sukses
						<?php if($this->cart->contents()) { ?>
							$('.login-message').removeClass('alert alert-warning text-center').addClass('alert alert-info text-center').html('Selamat Anda berhasil login, anda akan dialihkan ke keranjang belanja anda.');
							setTimeout(function(){ window.location=base+"keranjang" },1200);
							<?php } else  { ?>
								$('.login-message').removeClass('alert alert-warning text-center').addClass('alert alert-info text-center').html(msg.isi);
								setTimeout(function(){ window.location=base+"user" },1200);
								<?php } ?>
							}
							else
							{
						//jika login gagal maka muncul pesan error
						$('.login-message').addClass('alert alert-warning text-center').html(msg.isi);
					}
				},
				complete:function(){
					$("#btn-login").html('Login <i class="fa fa-sign-in"></i>');
				}
			});
});
$("#form-lupa form").submit(function() {
			// menampung data
			var base = '<?php echo base_url(); ?>';
			data = $("#form-lupa form").serialize();
			$("#btn-reset").html('Memproses...');
			$.ajax({
				url: $(this).prop('action'),
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(msg) {
					if(msg.success==true) { //jika login berhasil maka muncul pesan sukses
						$('.login-message').removeClass('alert alert-warning text-center').addClass('alert alert-info').html(msg.isi);
						setTimeout(function(){ window.location=base+"user" },3500);
					}
					else
					{
						//jika login gagal maka muncul pesan error
						$('.login-message').addClass('alert alert-warning text-center').html(msg.isi);
					}
				},
				complete:function(){
					$("#btn-reset").html('Reset Password <i class="fa fa-sign-in"></i>');
				}
			});
		});
$(".signup-form form").submit(function() {
			// menampung data
			var base = '<?php echo base_url(); ?>';
			data = $(".signup-form form").serialize();
			$("#btn-signup").html('Memproses...');
			$.ajax({
				url: $(this).prop('action'),
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(msg) {
					if(msg.success==true) { //jika login berhasil maka muncul pesan sukses
						$('.login-message').removeClass('alert alert-warning text-center').addClass('alert alert-info').html(msg.isi);
						$(':input').val('');
						setTimeout(function(){ window.location=base+"user" },3500);
					}
					else
					{
						//jika login gagal maka muncul pesan error
						$('.login-message').addClass('alert alert-warning text-center').html(msg.isi);
					}
				},
				complete:function(){
					$("#btn-signup").html('Daftar <i class="fa fa-sign-in"></i>');
				}
			});
		});
});
</script>
