<div class="col-sm-9 padding-right">
	<h2 class="title text-center">Detail Produk</h2>
	<div class="product-details"><!--product-details-->
		<div class="col-sm-5">
			<div class="view-product">
				<img src="<?php echo base_url('asset/images/produk').'/'.$gambar; ?>" alt="<?php echo $nama_produk; ?>" style="height:300px;">
				<h3><a style="color:#000;" href="<?php echo base_url('asset/images/produk').'/'.$gambar; ?>">ZOOM</a></h3>
			</div>
		</div>
		<div class="col-sm-7">
			<div class="product-information"><!--/product-information-->
				<h2><?php echo $nama_produk; ?></h2>
				<p><b>Kode Produk :</b> <?php echo $kode; ?></p>
				<span>
					<span><?php echo 'Rp ' .number_format($harga, 2); ?></span>
					<p>
						<?php echo form_open('keranjang/tambah_barang'); ?>
						<label>Jumlah Pesanan :</label>
						<input type="hidden" name="id" value="<?php echo $kode; ?>">
						<input type="hidden" name="price" value="<?php echo $harga; ?>">
						<input type="hidden" name="name" value="<?php echo $nama_produk; ?>">
						<input type="hidden" name="pic" value="<?php echo $gambar; ?>">
						<input type="hidden" name="berat" value="<?php echo $berat; ?>">
						<input name="qty" value="1" type="text">
						<button type="submit" class="btn btn-fefault cart">
							<i class="fa fa-shopping-cart"></i>
							Beli
						</button>
					</p>
					<?php echo form_close(); ?>
				</span>
				<p><b>Berat :</b> <?php echo $berat.' kg'; ?></p>
				<p><b>Stok Barang :</b> Tersedia</p>
				<p><b>Kondisi :</b> Baru</p>
			</div><!--/product-information-->
			<br>
			<div class="fb-like" data-width="100%" data-href="<?php echo current_url(); ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
		</div>
	</div><!--/product-details-->
	<div class="recommended_items"><!--recommended_items-->
		<h2 class="title text-center">Produk Pilihan</h2>
		<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="item active">
					<?php foreach ($produk_active->result_array() as $prd) {
						$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
						$nama_produk = strtolower(str_replace($d,"",$prd['nama_produk']));
						?>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img height="200" alt="<?php echo $prd['nama_produk']; ?>" src="<?php echo base_url(); ?>asset/images/produk/<?php echo $prd['gambar']; ?>" alt="" />
										<h4><?php echo 'Rp '. number_format($prd['harga'], 2); ?></h4>
										<h5><?php echo $prd['nama_produk']; ?></h5>
										<a href="<?php echo base_url('produk/detail').'/'.$prd['kode_produk'].'/'.str_replace(' ', '-', strtolower($nama_produk)).'.html'; ?>" class="btn btn-default add-to-cart">Lihat Detail</a>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="item">
						<?php foreach ($produk_item->result_array() as $prd) { 
							$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
							$nama_produk = strtolower(str_replace($d,"",$prd['nama_produk']));
							?>
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<img height="200" alt="<?php echo $prd['nama_produk']; ?>" src="<?php echo base_url(); ?>asset/images/produk/<?php echo $prd['gambar']; ?>" alt="" />
											<h4><?php echo 'Rp '. number_format($prd['harga'], 2); ?></h4>
											<h5><?php echo $prd['nama_produk']; ?></h5>
											<a href="<?php echo base_url('produk/detail').'/'.$prd['kode_produk'].'/'.str_replace(' ', '-', strtolower($nama_produk)).'.html'; ?>" class="btn btn-default add-to-cart">Lihat Detail</a>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
					<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>
					<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>			
				</div>
			</div>
		</div>