<section id="sidebar"> <!-- kategori -->
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<h2>Keranjang</h2>
					<div class="panel panel-default">
						<div class="panel-body">
							<h4> <i class="fa fa-shopping-cart"></i> : <?php echo  $jml_produk = $this->cart->contents() ? count($this->cart->contents()).' Produk':'Keranjang Kosong'; ?>  </h4> 
						</div>
						<div class="panel-footer"><h4><?php echo $harga_produk = $this->cart->contents() ? 'Total Rp '.number_format($this->cart->total(), 2):''; ?></h4></div>
					</div>
					<h2>Kategori</h2>
					<?php if($kategori->num_rows() == 0) { ?>
					<div class="panel panel-default">
						<div class="panel-body text-center">
							<h4>Kategori Kosong</h4> 
						</div>
					</div>
					<?php } ?>
					<div class="panel-group category-products" id="accordian">
						<?php $no=1; foreach ($kategori->result_array() as $key => $value) { 
							$sub_kat = $this->db->get_where('toa_kategori', array('kode_level'=>1, 'kode_parent'=>$value['id_kategori']) );
							?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<?php 
										echo form_open('produk/pencarian');
										$row = $sub_kat->row_array();
										$parent = $row['kode_parent']; 
										echo form_hidden('cari', $value['id_kategori'] );
										if($value['id_kategori'] != $parent )  { ?>
										<button type="submit" class="btn" style="width:100%;">
											<?php } else { ?>
											<button class="btn" style="width:100%;" data-toggle="collapse" data-parent="#accordian" href="#<?php echo $no;  ?>">
												<?php }
												if($value['id_kategori'] == $parent )  { ?>
												<span class="badge pull-right"><i class="fa fa-plus"></i></span>
												<?php }  ?>
												<?php echo $value['nama_kategori']; ?>
											</button>
											<?php echo form_close(); ?>
										</h4>
									</div>
									<div id="<?php echo $no; ?>" class="panel-collapse collapse">
										<div class="panel-body">
											<ul><li><?php echo form_open('produk/pencarian'); echo form_hidden('cari', $value['id_kategori'] ); ?><button type="submit" class="btn btn-warning"><?php echo $value['nama_kategori']; ?></button><?php echo form_close(); ?></li></ul>
											<?php foreach ($sub_kat->result_array() as $key2 => $value2) { ?>
											<ul>
												<li><?php echo form_open('produk/pencarian'); echo form_hidden('cari', $value2['id_kategori'] ); ?><button class="btn" style="width:100%;" type="submit"><?php echo $sub =  isset($value2['nama_kategori'])?$value2['nama_kategori']:'Belum Ada Sub Kategori'; ?> </button><?php echo form_close(); ?></li>
											</ul>
											<?php } ?>
										</div>
									</div>
								</div>
								<?php 
								$no++; } ?>
							</div>
							<h2>Info</h2>
							<div class="panel panel-default">
								<div class="panel-heading"></div>
								<ul class="list-group">
									<li><a class="list-group-item" href="<?php echo base_url('bantuan'); ?>">Tentang Kami</a></li>
									<li><a class="list-group-item" href="<?php echo base_url('bantuan'); ?>">Kebijakan Privasi</a></li>
									<li><a class="list-group-item" href="<?php echo base_url('bantuan'); ?>">Retur Produk</a></li>
								</ul>
								<div class="panel-footer"></div>
							</div>
						</div>
					</div>