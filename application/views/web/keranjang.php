<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>">Home</a></li>
				<li class="active">Keranjang Belanja</li>
			</ol>
		</div>
		<?php 
		if(!$this->cart->contents()) {
			echo '<div class="panel panel-default">
			<div class="panel-heading"></div>
			<div class="panel-body text-center">
			<h3>Keranjang Belanja Anda Masih Kosong</h3>
			</div>
			<div class="panel-footer"></div>
			</div> ';
		} else {
			?>
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>Tips :</strong><br>
				1. Apabila Anda mengubah jumlah (Qty), jangan lupa tekan tombol Update Keranjang Belanja. <br>
				2. Untuk menghapus barang pada keranjang belanja, silahkan klik tombol Hapus. <br>
				3. Total harga di atas belum termasuk ongkos kirim yang akan dihitung saat Selesai Belanja.<br> 
				4. Harga belum termasuk ongkir, detail pembayaran akan kami beritahukan lewat email setelah kami menerima pesanan anda. <br>
				5. Apabila sehari setelah pemesanan anda belum menerima email pemberitahuan dari kami, silakan hubungi kami melalui email atau no. telp yang ada pada website ini.
			</div>
			<?php echo form_open('keranjang/update_keranjang', array('class'=>'formUpdate')); ?>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Nama Barang</td>
							<td class="description"></td>
							<td class="price">Harga</td>
							<td class="quantity">Jumlah</td>
							<td></td>
							<td class="total">Sub Total</td>
							<td>Hapus</td>
						</tr>
					</thead>
					<tbody>
						<?php $no=1; foreach ($this->cart->contents() as $items) { ?>
						<?php echo form_hidden('rowid[]', $items['rowid']); ?>
						<tr>
							<td class="cart_product">
								<img height="100" src="<?php echo base_url('asset/images/produk/').'/'.$items['pic']; ?>" alt="<?php echo $items['name'] ?>">
							</td>
							<td class="cart_description">
								<h4><a href=""><?php echo $items['name'] ?></a></h4>
								<p><b>Kode :</b><?php echo $items['id'] ?></p>
							</td>
							<td class="cart_price">
								<p>Rp <?php echo $this->cart->format_number($items['price']); ?></p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<select class="btn btn-default" name="qty[]">
										<?php 
										for($i=1;$i<=50;$i++)
										{
											if($i==$items['qty'])
											{
												echo "<option selected>".$items['qty']."</option>";
											}
											else
											{
												echo "<option>".$i."</option>";
											}
										}	
										?>
									</select>
								</div>
							</td>
							<td></td>
							<td class="cart_total">
								<p class="cart_total_price">Rp <?php echo $this->cart->format_number($items['subtotal']); ?></p>
							</td>
							<td class="cart_delete">
								<a title="Hapus produk" class="cart_quantity_delete" href="<?php echo base_url('keranjang/hapus_item').'/'.$items['rowid']; ?>"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div class="panel-body">
						<h3 class='total_bayar'>Total Rp <?php echo $this->cart->format_number($this->cart->total()); ?></h3>
						<p class="pull-right">
							<?php if($this->session->userdata('isLoggedIn')!='userTOA') { ?>
							<a href="<?php echo base_url('keranjang/cekLogin'); ?>" class="btn btn-primary">Selesai (Check Out)</a>
							<?php } ?>
							<input type="submit" class="btn btn-primary" value="Update Keranjang" />
						</p>
					</div>
					<div class="panel-footer"></div>
				</div>
			</div>
			<?php if($this->session->flashdata('result')) { ?>
			<div class="message alert alert-warning"><?php echo $this->session->flashdata('result'); ?></div>
			<?php } ?>
			<?php 
			echo form_close(); 
			if($this->session->userdata('isLoggedIn')=='userTOA') { ?>
			<div id="chekoutmsg"></div>
			<?php echo form_open('checkout', array('role'=>'form', 'class'=>'formCheckout', 'onsubmit'=>'return false')); ?>
			<legend><i class="fa fa-user"></i> Detail Penerima</legend>
			<input type="hidden" name="kode_member" value="<?php echo $this->session->userdata('kode'); ?>">
			<div class="form-group">
				<input required type="text" name="nama_penerima" class="form-control" id="" placeholder="Masukan Nama Penerima">
			</div>
			<div class="form-group">
				<input required type="text" name="alamat1" class="form-control" id="" placeholder="Masukan Alamat Lengkap Penerima">
			</div>
			<div class="form-group">
				<select required name="provinsi" id="provinsi" class="form-control">
					<option value="">Pilih Provinsi</option>
					<?php foreach ($lokasi->result_array() as $value) { ?>
					<option  value="<?php echo $value['lokasi_propinsi']; ?>"><?php echo $value['lokasi_nama']; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<select required name="kota" id="data_kota" class="form-control">
					<option class="loading">Pilih Kabupaten/Kota</option>
				</select>
			</div>
			<div  class="form-group">
				<select required name="kecamatan" id="data_kecamatan" class="form-control">
					<option class="loading">Pilih Kecamatan</option>
				</select>
			</div>
			<div  class="form-group">
				<select required name="kelurahan" id="data_kelurahan" class="form-control">
					<option class="loading">Pilih Kelurahan/Desa</option>
				</select>
			</div>
			<div class="form-group">
				<input required type="text" name="kode_pos" class="form-control"  placeholder="Masukan Kode Pos">
			</div>
			<div class="form-group">
				<input required type="text" class="form-control" name="no_telp" placeholder="Masukan No Telp">
			</div>
			<legend><i class="fa fa-star"></i> Detail Pembayaran</legend>
			<h5><span class="glyphicon glyphicon-star"></span> Silakan catat detail bank yang akan anda pilih.</h5>
			<div class="form-group">
				<select required name="bank" class="form-control">
					<option value="">Pilih Bank Tujuan</option>
					<?php foreach ($bank->result_array() as $value) { ?>
					<option  value="<?php echo $value['kode_bank']; ?>"><?php echo $value['nama_bank'].' #No.Rekening : ('.$value['no_rekening'].')  #Atas Nama : '.$value['atas_nama']; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<select required name="kurir" class="form-control">
					<option value="">Pilih Jasa Pengiriman</option>
					<?php foreach ($kurir->result_array() as $value) { ?>
					<option  value="<?php echo $value['kode_kurir']; ?>"><?php echo $value['nama_kurir']; ?></option>
					<?php } ?>
				</select>
			</div>
			<button type="submit" id="btn-out" class="btn btn-primary">Kirim Pesanan (Check Out)</button>
			<br><br>
		</form>
		<?php echo form_close(); }?>
	</div>
	<?php } ?>
</section>
<script type="text/javascript">
$(".formCheckout").submit(function() {
			// menampung data
			var base = '<?php echo base_url(); ?>';
			data = $(this).serialize();
			$("#btn-out").html('Memproses...');
			$.ajax({
				url: $(this).prop('action'),
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(msg) {
					if(msg.stat == true) { 
						$('.formCheckout').hide();
						$("#chekoutmsg").removeClass('alert alert-warning text-center').addClass('alert alert-info text-center').html(msg.isi);
						setTimeout(function(){ window.location=base+"user" },12000);
					}
					else
					{
						$("#chekoutmsg").addClass('alert alert-warning text-center').html(msg.isi);
					}
				},
				complete:function(){
					$("#btn-out").html('Kirim Pesanan <i class="fa fa-sign-in"></i>');
				}
			});
		});
$('#provinsi').change(function(event) {
	var id = $(this).val();
	$.ajax({
		url:"<?php echo base_url();?>keranjang/data_kota_ajax/",
		data:'id='+id,
		success:function(data){
			$("#data_kota").html(data);
		}
	})
});
$('#data_kota').change(function(event) {
	var prov = $('#provinsi').val();
	var kota = $(this).val();
	$.ajax({
		url:"<?php echo base_url();?>keranjang/data_kec_ajax/",
		data:'prov='+prov+'&kota='+kota,
		success:function(data){
			$("#data_kecamatan").html(data);
		}
	})
});
$('#data_kecamatan').change(function(event) {
	var prov = $('#provinsi').val();
	var kota = $('#data_kota').val();
	var desa = $(this).val();
	var nama_prov = $('#provinsi option:selected').text();
	var nama_kota = $('#data_kota option:selected').text();
	var nama_desa = $('#data_kecamatan option:selected').text();
	$.ajax({
		url:"<?php echo base_url();?>keranjang/data_kel_ajax/",
		data:'prov='+prov+'&kota='+kota+'&desa='+desa+'&nama_prov='+nama_prov+'&nama_kota='+nama_kota+'&nama_desa='+nama_desa,
		success:function(data) {
			$("#data_kelurahan").html(data);
		}
	})
});
</script>
