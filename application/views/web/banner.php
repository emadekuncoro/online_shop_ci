<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<?php 
							foreach ($banner->result() as $value) {
								$status = ($value->status==1)?'item active':'item';
								?>
								<div class="<?php echo $status; ?>">
									<div class="col-sm-6">
										<h1><span><?php $q=$this->db->get('toa_info'); $row = $q->row_array(); echo $nama = isset($row['nama_toko'])?$row['nama_toko']:''; ?></span</h1>
										<h2><?php echo $value->judul; ?></h2>
										<p><?php echo $value->deskripsi; ?> </p>
										<a href="<?php echo base_url('produk'); ?>" > <button type="button" class="btn btn-default get">Lihat Semua Produk</button></a>
									</div>
									<div class="col-sm-6">
										<img src="<?php echo base_url(); ?>asset/images/home/<?php echo $value->gambar; ?>" class="girl img-responsive" alt="" />
									</div>
								</div>
								<?php } ?>
							</div>
							<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section><!--/slider-->