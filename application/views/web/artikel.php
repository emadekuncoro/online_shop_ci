<div class="col-sm-8">
	<div class="blog-post-area">
		<h2 class="title text-center">Artikel Terbaru</h2>
		<?php foreach ($artikel->result() as $value) { 
			$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
			$judul = strtolower(str_replace($d,"",$value->judul));
			?>
			<div class="single-blog-post">
				<header><a href="<?php echo base_url('artikel/detail').'/'.strtolower($value->kode_artikel).'-'.str_replace(' ', '-', strtolower($judul)).'.html'; ?>" ><h3><?php echo $value->judul; ?></h3></a></header>
				<div class="post-meta">
					<ul>
						<li><i class="fa fa-user"></i> Arum Diyah P</li>
						<li><i class="fa fa-clock-o"></i><?php echo $value->tanggal;  ?></li>
						<li><i class="fa fa-calendar"></i><?php echo $value->jam; ?></li>
					</ul>
					<span>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star-half-o"></i>
					</span>
				</div>
				<p class="text-">
					<img  height="320" width="750" src="<?php echo base_url('asset/images/artikel').'/'.$value->gambar; ?>" alt="<?php echo $value->judul; ?>">
				</p>
				<summary><p><?php echo substr($value->isi_artikel, 0, 200).'...'; ?></p></summary>
				<a class="btn btn-primary" href="<?php echo base_url('artikel/detail').'/'.$value->kode_artikel.'/'.str_replace('-' , '/', $value->tanggal) .'/'.str_replace(' ', '-', strtolower($judul)).'.html'; ?>">Teruskan Baca</a>
				<br>
			</div>
			<?php } ?>
		</div>
		<br>
		<div class="text-center">
			<ul class="pagination">
				<?php echo $paginator; ?>
			</ul>
		</div>
	</div>