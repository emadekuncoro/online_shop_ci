<div class="col-sm-9">
	<h2 class="title text-center">Artikel</h2>
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="blog-post-area">
				<div class="single-blog-post">
					<header><h3><?php echo $judul; ?></h3></header>
					<div class="post-meta">
						<ul>
							<li><i class="fa fa-user"></i> Arum Diyah P</li>
							<li><i class="fa fa-clock-o"></i> <?php echo $tanggal; ?></li>
							<li><i class="fa fa-calendar"></i> <?php echo $jam; ?></li>
						</ul>
						<span>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
						</span>
					</div>
					<img width="100%" height="450" src="<?php echo base_url('asset/images/artikel').'/'.$gambar; ?>" alt="<?php echo $judul; ?>">
					<article>
						<p>
							<br>
							<?php echo $isi_artikel; ?>
						</p>
					</article>
				</div> <!-- single-blog-post -->
			</div><!--/blog-post-area-->
		</div> <!-- panel-body -->
		<div class="panel-footer">
			<div class="rating-area">
				<ul class="ratings">
					<li class="rate-this">Rating:</li>
					<li>
						<i class="fa fa-star color"></i>
						<i class="fa fa-star color"></i>
						<i class="fa fa-star color"></i>
						<i class="fa fa-star color"></i>
						<i class="fa fa-star"></i>
					</li>
				</ul>
				<ul class="tag">
					<li>KATEGORI :</li>
					<li><a class="color" href="<?php echo base_url('artikel'); ?>">ARTIKEL</a></li>
				</ul>
			</div><!--/rating-area-->
			<br>
			<div class="fb-like" data-width="100%" data-href="<?php echo current_url(); ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
		</div> <!-- panel-footer -->
	</div> <!-- panel-default -->
	<div class="fb-comments" data-width="100%" data-href="<?php echo current_url(); ?>" data-numposts="5" data-colorscheme="light"></div>
</div>