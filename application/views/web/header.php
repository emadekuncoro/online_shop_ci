<!DOCTYPE html>
<html lang="id">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" /> 
	<meta name="author" content="Emade Haryo Kuncoro">
	<meta name="keywords" content="<?php $q=$this->db->get('toa_info'); $row = $q->row_array(); echo $keywords = isset($row['keywords'])?$row['keywords']:''; ?>"> 
	<meta name="description" itemprop="description" content="<?php echo $deskripsi = isset($deskripsi)? $deskripsi : $row['deskripsi']; ?>">
	<meta property="og:title" content="<?php echo $row['nama_toko'].' - '.$deskripsi; ?>"/>
	<meta property="og:description" content="<?php echo $deskripsi; ?>"/>
	<meta property="og:site_name" content="<?php echo $row['nama_toko']; ?>"/>
	<meta property="og:url" content="<?php echo base_url(); ?>"/>
	<title>
		<?php
		$judul = isset($judul) ? $judul : '';
		echo $title = isset($row['nama_toko'])? $judul.$row['nama_toko']:''; 
		?>
	</title>
	<link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/responsive.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/jquery-ui.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>asset/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/main.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/html5shiv.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/jquery.scrollUp.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/jquery-ui.min.js"></script>
	<style type="text/css">
	h5,h4,h3 {color:#FE980F;}
	</style>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#all-product').hide().fadeIn(500);
		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
	});
	</script>
</head><!--/head-->
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.3&appId=1613979765509017";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> <?php echo $no_telp = isset($row['no_telp']) ? $row['no_telp']:''; ?></a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> <?php echo $email = isset($row['email']) ? $row['email']:''; ?></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="<?php echo $fb = isset($row['fb'])?$row['fb']:''; ?>"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?php echo $twitter=isset($row['twitter'])?$row['twitter']:''; ?>"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?php echo $google = isset($row['google'])?$row['google']:''; ?>"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="<?php echo base_url(); ?>"><img src="<?php echo $logo = isset($row['logo']) ? base_url('asset/images/home/'.$row['logo'].''):''; ?>" alt="<?php echo $deskripsi = isset($row['deskripsi'])?$row['deskripsi']:''; ?>" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href="<?php echo base_url('bantuan'); ?>"><i class="fa fa-star"></i> Cara Belanja</a></li>
								<li><a href="<?php echo base_url('keranjang'); ?>"><i class="fa fa-shopping-cart"></i> Keranjang <?php echo $barang = $this->cart->contents() ?'('. count($this->cart->contents()).')':''; ?></a></li>
								<?php if($this->session->userdata('isLoggedIn')=='userTOA') { ?>
								<li><a href="<?php echo base_url('user'); ?>"><i class="fa fa-user"></i> Halaman Member</a></li>
								<li><a href="<?php echo base_url('user/logout'); ?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
								<?php } else { ?>
								<li><a href="<?php echo base_url('user'); ?>"><i class="fa fa-lock"></i> Login</a></li>
								<li><a href="<?php echo base_url('user'); ?>"><i class="fa fa-plus"></i> Daftar</a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a class="home" href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
								<li><a class="all-product" href="<?php echo base_url('produk'); ?>"><i class="fa fa-shopping-cart"></i> Produk</a></li>
								<li><a href="<?php echo base_url('artikel'); ?>"><i class="fa fa-pencil"></i> Artikel</a></li>
								<li><a href="<?php echo base_url('testimoni'); ?>"><i class="fa fa-user"></i> Testimoni</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<?php echo form_open('produk/cari'); ?><input type="text" name="cari" placeholder="Cari Produk"/><?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	<div id="msg"></div>
