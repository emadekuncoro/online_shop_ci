<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>">Home</a></li>
				<li><a href="<?php echo base_url('user'); ?>">Member</a></li>
				<li class="active">Ubah Tujuan Pengiriman</li>
			</ol>
		</div>
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Detail Penerima saat ini : </strong><br> <?php echo 
			'Nama Penerima : '. $nama_penerima. '.<br>'.
			'Alamat  : <br>'. $alamat1. '.<br>'.$alamat2.
			'<br> <b>Silakan isi form di bawah ini jika anda ingin mengubah tujuan pengiriman.</b>'
			; ?>
		</div>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Perhatian!</strong> Jika anda mengubah alamat tujuan, maka kami akan memproses pesanan anda dari awal. <br>
			Sehingga anda harus menunggu kembali konfirmasi pesanan dari kami sebelum anda dapat melakukan konfirmasi pembayaran. Terima Kasih.
		</div>
		<div id="chekoutmsg"></div>
		<?php echo form_open('user/update_alamat', array('role'=>'form', 'class'=>'formCheckout', 'onsubmit'=>'return false')); ?>
		<legend><i class="fa fa-user"></i> Detail Penerima</legend>
		<input type="hidden" name="kode_order" value="<?php echo $kode; ?>">
		<div class="form-group">
			<input required  type="text" name="nama_penerima" class="form-control" id="" placeholder="Masukan Nama Penerima">
		</div>
		<div class="form-group">
			<input required type="text" name="alamat1" class="form-control" id="" placeholder="Masukan Alamat Lengkap Penerima">
		</div>
		<div class="form-group">
			<select required name="provinsi" id="provinsi" class="form-control">
				<option value="">Pilih Provinsi</option>
				<?php  foreach ($lokasi->result_array() as $value) { ?>
				<option  value="<?php echo $value['lokasi_propinsi']; ?>"><?php echo $value['lokasi_nama']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group">
			<select required name="kota" id="data_kota" class="form-control">
				<option class="loading">Pilih Kabupaten/Kota</option>
			</select>
		</div>
		<div  class="form-group">
			<select required name="kecamatan" id="data_kecamatan" class="form-control">
				<option class="loading">Pilih Kecamatan</option>
			</select>
		</div>
		<div  class="form-group">
			<select required name="kelurahan" id="data_kelurahan" class="form-control">
				<option class="loading">Pilih Kelurahan/Desa</option>
			</select>
		</div>
		<div class="form-group">
			<input required type="text" name="kode_pos" class="form-control"  placeholder="Masukan Kode Pos">
		</div>
		<div class="form-group">
			<input required type="text" class="form-control" name="no_telp" placeholder="Masukan No Telp">
		</div>
		<button type="submit" id="btn-out" class="btn btn-primary">Update Alamat</button>
		<br><br>
	</form>
	<?php echo form_close(); ?>
</div>
</section>
<script type="text/javascript">
$(".formCheckout").submit(function() {
			// menampung data
			var base = '<?php echo base_url(); ?>';
			data = $(this).serialize();
			$("#btn-out").html('Memproses...');
			$.ajax({
				url: $(this).prop('action'),
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(msg) {
					if(msg.stat == true) { 
						$('.formCheckout').hide();
						$("#chekoutmsg").removeClass('alert alert-warning text-center').addClass('alert alert-info text-center').html(msg.isi);
						setTimeout(function(){ window.location=base+"user" },1500);
					}
					else
					{
						$("#chekoutmsg").addClass('alert alert-warning text-center').html(msg.isi);
					}
				},
				complete:function(){
					$("#btn-out").html('Update Alamat <i class="fa fa-sign-in"></i>');
				}
			});
		});
$('#provinsi').change(function(event) {
	var id = $(this).val();
	$.ajax({
		url:"<?php echo base_url();?>keranjang/data_kota_ajax/",
		data:'id='+id,
		success:function(data){
			$("#data_kota").html(data);
		}
	})
});
$('#data_kota').change(function(event) {
	var prov = $('#provinsi').val();
	var kota = $(this).val();
	$.ajax({
		url:"<?php echo base_url();?>keranjang/data_kec_ajax/",
		data:'prov='+prov+'&kota='+kota,
		success:function(data){
			$("#data_kecamatan").html(data);
		}
	})
});
$('#data_kecamatan').change(function(event) {
	var prov = $('#provinsi').val();
	var kota = $('#data_kota').val();
	var desa = $(this).val();
	var nama_prov = $('#provinsi option:selected').text();
	var nama_kota = $('#data_kota option:selected').text();
	var nama_desa = $('#data_kecamatan option:selected').text();
	$.ajax({
		url:"<?php echo base_url();?>keranjang/data_kel_ajax/",
		data:'prov='+prov+'&kota='+kota+'&desa='+desa+'&nama_prov='+nama_prov+'&nama_kota='+nama_kota+'&nama_desa='+nama_desa,
		success:function(data) {
			$("#data_kelurahan").html(data);
		}
	})
});
</script>
