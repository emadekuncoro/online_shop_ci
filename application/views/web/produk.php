<div id="all-product">
	<div class="col-sm-9 padding-right">
		<div class="features_items"><!--features_items-->
			<h2 class="title text-center">Semua Produk</h2>
			<?php 
			foreach ($produk->result_array() as $key => $value) { 
				$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
				$link = strtolower(str_replace($d,"",$value['nama_produk']));
				?>
				<div class="col-sm-4">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<img alt="<?php echo  $value['nama_produk']; ?>" height="210" src="<?php echo base_url(); ?>asset/images/produk/<?php echo $value['gambar']; ?>" alt="" />
								<h2><?php echo 'Rp '.number_format($value['harga'], 2); ?></h2>
								<p><?php echo  $value['nama_produk']; ?></p>
								<?php echo form_open('keranjang/tambah_barang'); ?>
								<input type="hidden" name="id" value="<?php echo $value['kode_produk']; ?>">
								<input type="hidden" name="price" value="<?php echo $value['harga']; ?>">
								<input type="hidden" name="name" value="<?php echo $value['nama_produk']; ?>">
								<input type="hidden" name="pic" value="<?php echo $value['gambar']; ?>">
								<input type="hidden" name="berat" value="<?php echo $value['berat']; ?>">
								<input type="hidden" name="qty" value="1">
								<button type="submit" class="btn btn-default add-to-cart">
									<i class="fa fa-shopping-cart"></i>
									Beli
								</button>
								<?php echo form_close(); ?>
							</div>
							<div class="product-overlay">
								<div class="overlay-content">
									<h2><?php echo 'Rp '.number_format($value['harga'], 2); ?></h2>
									<p><?php echo $value['nama_produk']; ?></p>
									<?php echo form_open('keranjang/tambah_barang'); ?>
									<input type="hidden" name="id" value="<?php echo $value['kode_produk']; ?>">
									<input type="hidden" name="price" value="<?php echo $value['harga']; ?>">
									<input type="hidden" name="name" value="<?php echo $value['nama_produk']; ?>">
									<input type="hidden" name="pic" value="<?php echo $value['gambar']; ?>">
									<input type="hidden" name="berat" value="<?php echo $value['berat']; ?>">
									<input type="hidden" name="qty" value="1">
									<button type="submit" class="btn btn-default add-to-cart">
										<i class="fa fa-shopping-cart"></i>
										Beli
									</button>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
						<div class="choose">
							<ul class="nav nav-pills nav-justified">
								<li><a href="<?php echo base_url('produk/detail').'/'.$value['kode_produk'].'/'.str_replace(' ', '-', strtolower($link)).'.html'; ?>"><i class="fa fa-plus-square"></i>Lihat Detail</a></li>
								<li><a href="<?php echo base_url('produk'); ?>"><i class="fa fa-plus-square"></i>Semua Produk</a></li>
							</ul>
						</div>
					</div>
				</div>
				<?php } ?>
			</div><!-- features item -->
			<ul class="pagination">
				<?php echo $paginator; ?>
			</ul>
		</div>
	</div>
