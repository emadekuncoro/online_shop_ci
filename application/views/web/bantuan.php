<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="<?php echo base_url(); ?>">Home</a></li>
				  <li class="active">Pusat Bantuan</li>
				</ol>
			</div>

 <div class="panel panel-default">
 	<div class="panel-heading">
 	</div>
	 	<div class="panel-group" id="accordion">
	 		<div class="panel panel-warning">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#tentang_kami">
          <i class="fa fa-star"></i> Tentang Kami
        </a>
      </h4>
    </div>
    <div id="tentang_kami" class="panel-collapse collapse">
      <div class="panel-body">
      	<div class="alert alert-warning"><?php echo $tentang_kami; ?></div>
      </div>
    </div>
  </div>
  <div class="panel panel-warning">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#cara_belanja">
          <i class="fa fa-star"></i> Cara Belanja
        </a>
      </h4>
    </div>
    <div id="cara_belanja" class="panel-collapse collapse">
      <div class="panel-body">
      	<div class="alert alert-warning"><?php echo $cara_belanja; ?></div>
      </div>
    </div>
  </div>
  <div class="panel panel-warning">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#kebijakan_privasi">
          <i class="fa fa-star"></i> Kebijakan Privasi
        </a>
      </h4>
    </div>
    <div id="kebijakan_privasi" class="panel-collapse collapse">
      <div class="panel-body">
      	<div class="alert alert-warning"><?php echo $kebijakan_privasi; ?></div>
      </div>
    </div>
  </div>
  <div class="panel panel-warning">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#retur_produk">
          <i class="fa fa-star"></i> Aturan Retur Produk
        </a>
      </h4>
    </div>
    <div id="retur_produk" class="panel-collapse collapse">
      <div class="panel-body">
      	<div class="alert alert-warning"><?php echo $retur_produk; ?></div>
      </div>
    </div>
  </div>
</div>
 	<div class="panel-footer"></div>
 </div>
</section>