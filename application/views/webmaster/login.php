<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Emade Haryo Kuncoro">
	<title><?php echo $title; ?></title>
	<!-- Bootstrap CSS -->
	<link href="<?php echo base_url('asset/css/bootstrap.css'); ?>" rel="stylesheet">
	<style>
	.panel {max-width:420px;margin:1em auto;}
	.panel-footer {margin-top:3em;}
	.panel-heading {margin-bottom:2em;}
	</style>	
	<!-- jQuery -->
	<script src="<?php echo base_url('asset/js/jquery.min.js'); ?>"></script>
	<!-- Bootstrap JavaScript -->
	<script src="<?php echo base_url('asset/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('asset/js/login.js'); ?>"></script>
</head>
<body>
	<div class="panel panel-primary">
		<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Login Admin</div>
		<div class="panel-body" id="login">
			<!-- panel start -->
			<?php echo form_open('webmaster/login', array('role'=>'form', 'autocomplete'=>'off', 'onsubmit'=>'return false')); ?>
				<div class="text-center"><img src="<?php echo base_url('asset/images/home/logo.png'); ?>" alt=""></div>
				<br/>
				<legend class="text-center"></legend>
				<div class="login-message"></div>
				<div class="form-group">
					<label for="">Username</label>
					<div class="input-group">
						<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
						<input type="text" name="username" class="form-control username" id="" placeholder="Masukan Username">
					</div>
				</div>
				<div class="form-group">
					<label for="">Password</label>
					<div class="input-group">
						<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
						<input type="password" name="password" class="form-control password" id="" placeholder="Masukan Password">
					</div>
				</div>	
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-share-alt"></span>	 Login</button>
			<?php echo form_close(); ?>
			<!-- panel end -->
		</div>
		<div class="panel-footer text-center"><?php echo $credit; ?></div>
	</div>
</body>
</html>