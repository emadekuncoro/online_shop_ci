<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/artikel'); ?>">Artikel</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open_multipart('webmaster/simpan_artikel', 'role="form"'); ?>
<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':''; ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
<?php echo $upload_error =  $this->upload->display_errors() ? '<div class="alert alert-warning">'.$this->upload->display_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-flag"></span> Tambah Data Artikel</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_artikel = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Judul</label>
		<input type="text" class="form-control" name="judul" placeholder="Masukan Judul artikel" value="<?php echo $judul= isset($judul) ? $judul:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Isi Artikel</label>
		<textarea name="isi"><?php echo $isi= isset($isi) ? $isi:''; ?></textarea>
	</div>
	<?php if(isset($gambar)) { ?>
	<div class="form-group">
		<label for="">Preview</label>
		<img class="img-responsive" src="<?php echo base_url('asset/images/artikel').'/'.$gambar=isset($gambar)?$gambar:''; ?>">
	</div>
	<?php } ?>
	<div class="form-group">
		<label for="">Upload Gambar</label>
		<input type="file" name="gb">
		<span class="help-block">Silakan pilih gambar.</span>
	</div>
	<input type="hidden" name="st" value="<?php echo $status = isset($st) ? $st:'tambah'; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/profil'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>
