<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li><a href="<?php echo base_url('webmaster/pembayaran') ?>">Pembayaran</a></li>
</ol>
<div class="well text-right">
	   
	   	<?php echo form_open('webmaster/cari_pembayaran', 'class="form-inline" role="form"'); ?>
	   
		<div class="form-group">
	   		<input type="text" style="width:420px;" required class="form-control" name="cari" placeholder="Masukan Tanggal Pembayaran. Exp:(yyyy-mm-dd) 2015-03-05">	   
	   	</div>
	   	<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
	   <?php echo form_close(); ?>
</div>	
<div class="alert alert-danger text-center">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Note :</strong> <br> 1. Konfirmasi Valid : Apabila data pembayaran sesuai dengan data pesanan yang ada.
							<br>	2. Konfirmasi Tidak Valid : Apabila data pembayaran tidak sesuai dengan data pesanan yang ada.
</div>
<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>Tgl Bayar</th>
			<th>Sejumlah</th>
			<th>No Rekening</th>
			<th>Atas Nama</th>
			<th>Bank Tujuan</th>
			<th>Status</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($pembayaran->num_rows()) >0 ? '':'<tr><td colspan="8"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($pembayaran->result_array() as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $this->Adminmodel->ubahTanggal($value['tgl_bayar']); ?></td>
				<td><?php echo 'Rp '. number_format($value['jml_bayar'], 2); ?></td>
				<td><?php echo $value['no_rek']; ?></td>
				<td><?php echo $value['atas_nama']; ?></td>
				<td><?php echo $value['bank_tujuan']; ?></td>
				<td><?php echo $value['status']; ?></td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php if($value['status'] != 'Lunas') { ?>
							<li><a onClick="return confirm('Anda yakin data pembayaran ini valid / sesuai ? (Klik OK untuk melanjutkan) ');" href="<?php echo base_url('webmaster/konfirmasi_pembayaran_valid').'/'.$value['kode_pembayaran'].'/'.$value['kode_order']; ?>"><span class="glyphicon glyphicon-ok"></span> Konfirmasi Valid</a></li>
							<li><a onClick="return confirm('Anda yakin data pembayaran ini tidak valid ? (Klik OK untuk melanjutkan) ');" href="<?php echo base_url('webmaster/konfirmasi_pembayaran_invalid').'/'.$value['kode_pembayaran'].'/'.$value['kode_order']; ?>"><span class="glyphicon glyphicon-remove"></span> Konfirmasi Tidak Valid</a></li>
							<?php } ?>
							<li><a href="<?php echo base_url('webmaster/detail_order').'/'.$value['kode_order']; ?>"><span class="glyphicon glyphicon-share-alt"></span> Detail Pesanan</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data pembayaran ini ?');" href="<?php echo base_url('webmaster/hapus_pembayaran').'/'.$value['kode_pembayaran']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

	<div class="pagination"><?php echo $paginator; ?></div>
	