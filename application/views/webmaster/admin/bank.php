<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li><a href="<?php echo base_url('webmaster/bank') ?>">Bank</a></li>
	<li><a id="tambah" href="<?php echo base_url('webmaster/tambah_bank');?>">Tambah Data</a></li>
</ol>


<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Bank</th>
			<th>No.Rekening</th>
			<th>Atas Nama</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($bank->num_rows()) >0 ? '':'<tr><td colspan="5"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($bank->result_array() as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $value['nama_bank']; ?></td>
				<td><?php echo $value['no_rekening']; ?></td>
				<td><?php echo $value['atas_nama']; ?></td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/edit_bank').'/'.$value['kode_bank']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_bank').'/'.$value['kode_bank']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

	<div class="pagination"><?php echo $paginator; ?></div>
	