<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/order'); ?>">Pesanan</a>
	</li>
	<li class="active">Kirim Resi Pengiriman</li>
</ol>

<?php echo form_open('webmaster/simpan_resi', 'role="form"'); ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-envelope"></span> Kirim Resi Pengiriman</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_order = isset($kode) ? $kode:''; ?>">
	<input type="hidden" name="email" value="<?php echo $email = isset($email) ? $email:''; ?>">
	<input type="hidden" name="alamat1" value="<?php echo $alamat1 = isset($alamat1) ? $alamat1:''; ?>">
	<input type="hidden" name="alamat2" value="<?php echo $alamat2 = isset($alamat2) ? $alamat2:''; ?>">
	<input type="hidden" name="sub_total" value="<?php echo $sub_total = isset($sub_total) ? $sub_total:''; ?>">
	<input type="hidden" name="ongkir" value="<?php echo $ongkir = isset($ongkir) ? $ongkir:''; ?>">
	<input type="hidden" name="total_bayar" value="<?php echo $total_bayar = isset($total_bayar) ? $total_bayar:''; ?>">
	<div class="form-group">
		<label for="">Tanggal Pengiriman</label>
		<input required type="date" class="form-control datepicker"  name="tgl_kirim" value="">
	</div>
	<div class="form-group">
		<label for="">Nama Kurir</label>
		<input required type="text" class="form-control"  name="nama_kurir" value="<?php echo $nama_kurir = isset($nama_kurir) ? $nama_kurir:''; ?>">
	</div>
	<div class="form-group">
		<label for="">No. Resi</label>
		<input required type="text"  class="form-control" name="no_resi" value="">
	</div>
	

	<button type="submit" class="btn btn-primary">Kirim Resi</button>
	<a href="<?php echo base_url('webmaster/order'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>

<script type="text/javascript">
jQuery(document).ready(function($) {
		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
});
</script>