<div id="mainCont">
<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li><a href="<?php echo base_url('webmaster/artikel') ?>">Artikel</a></li>
	<li><a id="tambah" href="<?php echo base_url('webmaster/tambah_artikel');?>">Tambah Data</a></li>
</ol>

<div class="well text-right">
	   	<?php echo form_open('webmaster/cari_artikel', 'class="form-inline" role="form"'); ?>
		<div class="form-group">
	   		<input type="text" class="form-control" required name="cari" placeholder="Cari Judul Artikel ">	   
	   	</div>
	   	<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
	   <?php echo form_close(); ?>
</div>	

<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>


<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Post</th>
			<th>Judul</th>
			<th>Dibaca</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($artikel->num_rows()) >0 ? '':'<tr><td colspan="5"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($artikel->result_array() as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo '<strong>'.$this->Adminmodel->ubahTanggal($value['tanggal']).'</strong>'; ?></td>
				<td><?php echo '<strong>'.$value['judul'].'</strong>'; ?></td>
				<td><?php echo $value['dibaca']; ?></td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/edit_artikel').'/'.$value['kode_artikel']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_artikel').'/'.$value['kode_artikel']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>
<div id="pagNav" class="pagination"><?php echo $paginator; ?></div>
</div>
	
