<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li class="active">Banner</li>
	<li><a id="tambah" href="<?php echo base_url('webmaster/tambah_banner');?>">Tambah Data</a></li>
</ol>
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Tips : </strong> Pilihan <strong>Jadikan Utama</strong> merupakan pilihan untuk menjadikan banner yang dipilih sebagai banner yang pertama muncul saat halaman website dimuat.
</div>
<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Judul</th>
			<th>Deskripsi</th>
			<th>Preview</th>
			<th><span class="glyphicon glyphicon-star"></span></th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($banner->num_rows()) >0 ? '':'<tr><td colspan="5"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($banner->result_array() as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $value['judul']; ?></td>
				<td><?php echo $value['deskripsi']; ?></td>
				<td><img class="img-rounded" height="100px" src="<?php echo base_url('asset/images/home').'/'.$value['gambar']; ?>"></td>
				<td><?php echo $status = ($value['status']==1)?'<span class="glyphicon glyphicon-star"></span>':'<span class="glyphicon glyphicon-star-empty"></span>'; ?></td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php if($value['status']!=1) { ?><li><a href="<?php echo base_url('webmaster/set_banner').'/'.$value['kode_banner']; ?>"><span class="glyphicon glyphicon-star"></span> Jadikan Utama</a></li><?php } ?>
							<li><a href="<?php echo base_url('webmaster/edit_banner').'/'.$value['kode_banner']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<?php if($value['status']!=1) { ?><li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_banner').'/'.$value['kode_banner']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li><?php } ?>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

	<div class="pagination"><?php echo $paginator; ?></div>
	