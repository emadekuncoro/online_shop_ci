<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/banner'); ?>">Banner</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open_multipart('webmaster/simpan_banner', 'role="form"'); ?>
<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':''; ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
<?php echo $upload_error =  $this->upload->display_errors() ? '<div class="alert alert-warning">'.$this->upload->display_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-flag"></span> Tambah Data Banner</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_banner = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Judul</label>
		<input type="text" class="form-control" name="judul" placeholder="Masukan Judul Banner" value="<?php echo $judul= isset($judul) ? $judul:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Deskripsi</label>
		<textarea name="deskripsi"><?php echo $deskripsi= isset($deskripsi) ? $deskripsi:''; ?></textarea>
	</div>
	<?php if(isset($gambar)) { ?>
	<div class="form-group">
		<label for="">Preview</label>
		<img class="img-responsive" src="<?php echo base_url('asset/images/home').'/'.$gambar=isset($gambar)?$gambar:''; ?>">
	</div>
	<?php } ?>
	<div class="form-group">
		<label for="">Upload Gambar</label>
		<input type="file" name="gb">
		<span class="help-block">Silakan pilih gambar.</span>
	</div>
	<input type="hidden" name="st" value="<?php echo $st = isset($st) ? $st:'tambah'; ?>">
	<input type="hidden" name="status" value="<?php echo $status; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/profil'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>

