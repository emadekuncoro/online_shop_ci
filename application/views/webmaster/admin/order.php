<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li><a href="<?php echo base_url('webmaster/order') ?>">Pesanan</a></li>
</ol>

<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Status</strong> <br>
	1. Pending 		: Pesanan belum dikonfirmasi dan pemesan belum mendapat email pemberitahuan jumlah pembayaran. <br>
	2. Belum Bayar 	: Pesanan sudah dikonfirmasi, email pemberitahuan pembayaran sudah dikirim. Pemesan belum mengkonfirmasi pembayaran. <br>
	3. Proses 		: Pemesan sudah mengkorfirmasi pembayaran. Status pembayaran masih pending atau menunggu konfirmasi. <br>
	4. Lunas 		: Pesanan sudah dikonfirmasi dan dibayar. Barang dalam proses pengiriman. <br>
	5. Terkirim 	: Pesanan sudah dikonfirmasi dan dibayar. Barang sudah dikirim.
</div>
<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Pesan</th>
			<th>Nama Pemesan</th>
			<th>SubTotal</th>
			<th>Ongkir</th>
			<th>Total Bayar</th>
			<th>Status</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($order->num_rows() > 0 ) ? '':'<tr><td colspan="9"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($order->result_array() as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $value['tgl_order']; ?></td>
				<td><?php $nm_pmsan = explode('@', $value['email']); echo $nm_pmsan[0]; ?></td>
				<td><?php echo 'Rp '. number_format($value['sub_total'], 2); ?> </td>
				<td><?php echo $ok =  ($value['ongkir'] != 0) ? 'Rp '.number_format($value['ongkir'], 2) : '-' ; ?></td>
				<td><?php echo $tb = ($value['total_bayar'] != 0) ? 'Rp '.number_format($value['total_bayar'], 2) : '-' ; ?></td>
				<td><?php echo $value['status']; ?></td>
				<td>
					<div class="btn-group">
						<a class="btn btn-primary" href="<?php echo base_url('webmaster/detail_order').'/'.$value['kode_order']; ?>"><span class="glyphicon glyphicon-share-alt"></span> Detail</a>
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#"><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php if($value['status'] == 'Pending') { ?>
							<li><a href="<?php echo base_url('webmaster/konfirmasi_order').'/'.$value['kode_order']; ?>"><span class="glyphicon glyphicon-ok"></span> Konfirmasi</a></li>
							<?php } ?>
							<?php if($value['status'] == 'Lunas') { ?>
							<li><a href="<?php echo base_url('webmaster/input_resi').'/'.$value['kode_order']; ?>"><span class="glyphicon glyphicon-envelope"></span> Kirim Resi</a></li>
							<?php } ?>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_order').'/'.$value['kode_order']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

	<div class="pagination"><?php echo $paginator; ?></div>
	