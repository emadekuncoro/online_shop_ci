<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/kurir'); ?>">Kurir</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open('webmaster/simpan_kurir', 'role="form"'); ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-user"></span> Tambah Data kurir</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_kurir = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Nama kurir</label>
		<input type="text" class="form-control"  required name="nama_kurir" placeholder="Masukan Nama kurir" value="<?php echo $nama_kurir= isset($nama_kurir) ? $nama_kurir:''; ?>">
	</div>
	
	

	<input type="hidden" name="st" value="<?php echo $status = isset($st) ? $st:'tambah'; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/kurir'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>