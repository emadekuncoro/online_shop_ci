
 <div class="panel panel-default">
 	<div class="panel-heading"><h3><span class="glyphicon glyphicon-star"></span> Detail Pesanan</h3></div>
 	<div class="panel-body text-danger">
 		<table class="table table-bordered">
 			<thead>
 				<tr>
 					<th><code>Nama Penerima</code></th>
 					<th><code>Alamat</code></th>
 					<th rowspan="2"><code>Nama Produk</code></th>
 					<th rowspan="2"><code>Berat</code></th>
 					<th rowspan="2"><code>Harga</code></th>
 					<th rowspan="2"><code>Jumlah</code></th>
 				</tr>
 			</thead>
 			<tbody>
 					<?php foreach ($detail1->result() as $value) { ?>
 				<tr>
 					<td ><?php echo $value->nama_penerima; ?></td>
 					<td><?php echo $value->alamat1; ?></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<td></td>
 					<?php foreach ($detail2->result() as $value2) { ?>
 					<tr><td colspan="2"></td>
 					<td><?php echo $value2->nama_produk; ?></td>

 					<td><?php echo $value2->berat.' kg'; ?></td>
 					<td><?php echo 'Rp '. number_format($value2->harga, 2); ?></td>
 					<td><?php echo $value2->jumlah; ?></td>
 					
 					</tr>
 					<?php } ?>
 					<td colspan="4"><code>Sub Total</code></td><td colspan="2">Rp <?php echo number_format($value->sub_total, 2); ?></td>
 					<tr>
 					<td colspan="4"><code>Ongkir</code></td><td colspan="2">Rp <?php echo $ok = ($value->ongkir != 0) ? number_format($value->ongkir, 2):'-'; ?></td>
 					</tr>
 					<tr>
 					<td colspan="4"><code>Total Bayar</code></td><td colspan="2"><strong>Rp <?php echo $tb = ($value->total_bayar != 0) ? number_format($value->total_bayar, 2):'-'; ?></strong></td>
 					</tr>
 				</tr>
 					<?php  } ?>
 			</tbody>
 		</table>
 	</div>
 	<div class="panel-footer">
 		<a class="btn btn-info" href="<?php echo base_url('webmaster/order') ?>">Kembali Ke Menu Pesanan</a>
 		<a class="btn btn-info" href="<?php echo base_url('webmaster/pembayaranPending') ?>">Kembali Ke Menu Pembayaran</a>
 	</div>
 </div>
