<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/profil'); ?>">Profil</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open('webmaster/simpan_profil', 'role="form"'); ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-user"></span> Tambah Data Admin</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_admin = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Nama Lengkap</label>
		<input type="text" class="form-control" required name="nama" placeholder="Masukan Nama Lengkap" value="<?php echo $nama_admin= isset($nama) ? $nama:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Username</label>
		<input type="text" class="form-control" required name="username" placeholder="Masukan Username" value="<?php echo $username= isset($user_admin) ? $user_admin:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Password</label>
		<input type="text" class="form-control" required name="password" placeholder="Masukan Password">
	</div>
	<input type="hidden" name="st" value="<?php echo $status = isset($st) ? $st:'tambah'; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/profil'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>