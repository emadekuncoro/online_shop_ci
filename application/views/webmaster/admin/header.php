	<!DOCTYPE html>
	<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Selamat Datang <?php echo $this->session->userdata('username'); ?></title>

		<!-- Bootstrap CSS -->
		<link href="<?php echo base_url('asset')?>/css/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>asset/css/jquery-ui.css" rel="stylesheet">

		<!-- jQuery -->
		<script src="<?php echo base_url('asset')?>/js/jquery.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="<?php echo base_url('asset')?>/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>asset/js/jquery-ui.min.js"></script>

		<script type="text/javascript" src="<?php echo base_url('asset/js');?>/tinymce/tinymce.min.js"></script>
		<script>
		tinymce.init({
		    selector: "textarea",
		    theme: "modern",
		    height: 200,
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
		         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		         "save table contextmenu directionality emoticons template paste textcolor"
		   ],
		   content_css: "css/content.css",
		   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons", 
		   style_formats: [
		        {title: 'Bold text', inline: 'b'},
		        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
		        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
		        {title: 'Example 1', inline: 'span', classes: 'example1'},
		        {title: 'Example 2', inline: 'span', classes: 'example2'},
		        {title: 'Table styles'},
		        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		    ]
		 }); 
		</script>
		<style>
		body {margin:1em auto;padding:1em;}
		.panel {margin:3em auto;max-width:100%;}
		.panel-footer {margin-top:2em;}
		form {width:85%;margin:0 auto; }
		 
		</style>
	</head>
	<body>

		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a title='Beranda' class="navbar-brand" href="<?php echo base_url('webmaster');?>" ><span class="glyphicon glyphicon-map-marker"></span></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo base_url('webmaster/produk'); ?>">Produk</a></li>
					<li><a href="<?php echo base_url('webmaster/kategori') ?>">Kategori</a></li>
					<li><a href="<?php echo base_url('webmaster/member');?>"> Member</a></li>
					<li><a href="<?php echo base_url('webmaster/testimoni');?>"> Testimoni</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pembayaran <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/pembayaran'); ?>"><span class="glyphicon glyphicon-asterisk"></span> Semua</a></li>
							<li><a href="<?php echo base_url('webmaster/pembayaranPending') ?>"><span class="glyphicon glyphicon-minus"></span> Pending</a></li>
							<li><a href="<?php echo base_url('webmaster/pembayaranLunas') ?>"><span class="glyphicon glyphicon-ok"></span> Lunas</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pesanan <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/order'); ?>"><span class="glyphicon glyphicon-asterisk"></span> Semua Pesanan</a></li>
							<li><a href="<?php echo base_url('webmaster/orderPending') ?>"><span class="glyphicon glyphicon-minus"></span> Pending</a></li>
							<li><a href="<?php echo base_url('webmaster/orderBelumBayar') ?>"><span class="glyphicon glyphicon-ban-circle"></span> Belum Bayar</a></li>
							<li><a href="<?php echo base_url('webmaster/pembayaranPending') ?>"><span class="glyphicon glyphicon-refresh"></span> Proses</a></li>
							<li><a href="<?php echo base_url('webmaster/orderLunas') ?>"><span class="glyphicon glyphicon-ok"></span> Lunas</a></li>
							<li><a href="<?php echo base_url('webmaster/orderTerkirim') ?>"><span class="glyphicon glyphicon-flash"></span> Terkirim</a></li>
						</ul>
					</li>
					<li><a href="<?php echo base_url('webmaster/artikel') ?>">Artikel</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pengaturan <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/info'); ?>"><span class="glyphicon glyphicon-record"></span> Info Toko</a></li>
							<li><a href="<?php echo base_url('webmaster/bank'); ?>"><span class="glyphicon glyphicon-record"></span> Akun Bank</a></li>
							<li><a href="<?php echo base_url('webmaster/kurir'); ?>"><span class="glyphicon glyphicon-record"></span> Jasa Pengiriman</a></li>
							<li><a href="<?php echo base_url('webmaster/banner') ?>"><span class="glyphicon glyphicon-record"></span> Banner</a></li>
							<li class="divider"></li>
							<li><span class="help-block text-center"><span class="glyphicon glyphicon-cog"></span> Halaman</span></li>
							<li class="divider"></li>
							<li><a href="<?php echo base_url('webmaster/cara_belanja');?>"><span class="glyphicon glyphicon-record"></span> Cara Belanja</a></li>
							<li><a href="<?php echo base_url('webmaster/tentang_kami');?>"><span class="glyphicon glyphicon-record"></span> Tentang Kami</a></li>
							<li><a href="<?php echo base_url('webmaster/kebijakan_privasi');?>"><span class="glyphicon glyphicon-record"></span> Kebijakan Privasi</a></li>
							<li><a href="<?php echo base_url('webmaster/retur_produk');?>"><span class="glyphicon glyphicon-record"></span> Retur Produk</a></li>
							<li class="divider"></li>
							<li><span class="help-block text-center"><span class="glyphicon glyphicon-cog"></span> Perlindungan</span></li>
							<li class="divider"></li>
							<li><a href="<?php echo base_url('webmaster/backup');?>"><span class="glyphicon glyphicon-download"></span> Backup Data</a></li>
						</ul>
					</li>


				</ul>
				
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <?php echo $this->session->username; ?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a target="_blank" href="<?php echo base_url('web'); ?>"><span class="glyphicon glyphicon-globe"></span> Lihat Website</a></li>
							<li><a href="<?php echo base_url('webmaster/profil'); ?>"><span class="glyphicon glyphicon-wrench"></span> Pengaturan Profil</a></li>
							<li><a href="<?php echo base_url('webmaster/logout') ?>"><span class="glyphicon glyphicon-share-alt"></span> Keluar</a></li>
						</ul>
					</li>
					<li><a href=""></a></li>

				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>

		<div class="panel panel-primary">
			<div class="panel-heading"></div>
			<div class="panel-body">

				
