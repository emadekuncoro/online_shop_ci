<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li><a href="<?php echo base_url('webmaster/member') ?>">Member</a></li>
	<li><a id="tambah" href="<?php echo base_url('webmaster/tambah_member');?>">Tambah Data</a></li>
</ol>
<div class="well text-right">
	   
	   	<?php echo form_open('webmaster/cari_member', 'class="form-inline" role="form"'); ?>
	   
		<div class="form-group">
	   		<label class="sr-only" for="">label</label>
	   		<input type="text" required class="form-control" name="cari" placeholder="Cari Email Member">	   
	   	</div>
	   	<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
	   <?php echo form_close(); ?>
</div>	

<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>Email</th>
			<th>No.Telp</th>
			<th>Status</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($member->num_rows()) >0 ? '':'<tr><td colspan="5"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($member->result_array() as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $value['email']; ?></td>
				<td><?php echo $value['no_telp']; ?></td>
				<td><?php echo $status = $value['status'] == 1 ? 'Aktif':'Tidak Aktif'; ?></td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/edit_member').'/'.$value['kode_member']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_member').'/'.$value['kode_member']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

	<div class="pagination"><?php echo $paginator; ?></div>
	