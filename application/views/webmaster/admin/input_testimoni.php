<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/testimoni'); ?>">Testimoni</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open('webmaster/simpan_testimoni', 'role="form"'); ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-user"></span> Tambah Data Testimoni</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_testimoni = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Nama</label>
		<input type="text" class="form-control"  required name="nama" placeholder="Masukan nama" value="<?php echo $nama= isset($nama) ? $nama:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Email</label>
		<input type="email" class="form-control"  required name="email" placeholder="Masukan Email" value="<?php echo $email= isset($email) ? $email:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Isi</label>
		<textarea name="isi" class="form-control"><?php echo $isi= isset($isi) ? $isi:''; ?></textarea>
	</div>
	<div class="form-group">
		<label for="">Status</label>
		<select required name="status" class="form-control">
		<?php 
		$data = array('0','1');
		$status= isset($status) ? $status:''; 
		foreach ($data as $key => $value) {
			$selected = ($value == $status) ? "selected='selected'":'';
			echo "<option value='$value' $selected >"; echo $value2 = ($value==1)?'Aktif':'Tidak Aktif'; echo "</option>";
		}
		?>
		</select>
	</div>

	<input type="hidden" name="st" value="<?php echo $status = isset($st) ? $st:'tambah'; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/testimoni'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>