<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li><a href="<?php echo base_url('webmaster/testimoni') ?>">Testimoni</a></li>
	<li><a id="tambah" href="<?php echo base_url('webmaster/tambah_testimoni');?>">Tambah Data</a></li>
</ol>


<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal</th>
			<th>Nama</th>
			<th>Isi Testimoni</th>
			<th>Status</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($testimoni->num_rows()) >0 ? '':'<tr><td colspan="6"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($testimoni->result_array() as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php $temp = explode('_', $value['waktu']); echo $this->Adminmodel->ubahTanggal($temp[0]); ?></td>
				<td><?php echo $value['nama']; ?></td>
				<td width="50%"><?php echo $value['isi']; ?></td>
				<td><?php echo $status = $value['status'] == 1 ? 'Aktif':'Tidak Aktif'; ?></td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php if($value['status'] != 1) { ?>
							<li><a href="<?php echo base_url('webmaster/aktifkan_testi').'/'.$value['kode_testimoni']; ?>"><span class="glyphicon glyphicon-ok"></span> Aktifkan</a></li>
							<?php } else { ?>
							<li><a href="<?php echo base_url('webmaster/nonaktifkan_testi').'/'.$value['kode_testimoni']; ?>"><span class="glyphicon glyphicon-remove"></span> Non Aktifkan</a></li>
							<?php } ?>
							<li><a href="<?php echo base_url('webmaster/edit_testimoni').'/'.$value['kode_testimoni']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_testimoni').'/'.$value['kode_testimoni']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

	<div class="pagination"><?php echo $paginator; ?></div>
	