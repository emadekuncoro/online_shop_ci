<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adminmodel extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	public function cekLogin($data)
	{
		$query = $this->db->query("SELECT pass_admin, nama_admin FROM toa_admin WHERE user_admin='".$data['user_admin']."' AND status_admin='".$data['status_admin']."' ");
		//cek apakah data ditemukan
		if($query->num_rows() == 1)
		{
			$row = $query->row_array();
			if (password_verify($data['pass_admin'], $row['pass_admin'])) {
			    $sess_data = array("username" => $row['nama_admin'], "isLoggedIn" => "admin");
				$this->session->set_userdata($sess_data);
				return TRUE;
			} else {
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}


	public function lihat_tabel($table, $order, $by, $limit,$offset)
	{

		return $q = ($order!="") ? $this->db->query("SELECT * FROM $table ORDER BY $order $by LIMIT $offset,$limit"):$this->db->query("SELECT * FROM $table LIMIT $offset,$limit");
	}

	public function lihat_tabel_cari($table, $order, $by, $limit, $offset, $cari)
	{
		return $q =  $this->db->select('*')->like($cari)->limit($limit,$offset)->order_by($order,$by)->get($table);
	}

	public function hitung_isi_tabel($table)
	{
		return $q = $this->db->count_all($table);
	}

	public function hitung_isi_tabel_cari($table, $field, $kata)
	{
		$query = $this->db->query("SELECT * FROM $table where $field like '%".$kata."%' ");

		return $query->num_rows();
	}
	

	public function hapus_tabel_byId($table, $field, $id)
	{

		$q = $this->db->delete($table, array($field => $id) );
		$cek = $this->db->affected_rows();
		return $msg = ($cek == 1) ? $this->session->set_flashdata('result','Data Berhasil Dihapus'):$this->session->set_flashdata('result','Data Gagal Dihapus'); 
	}

	

	public function simpan_data($table, $data)
	{
		$q = $this->db->insert($table, $data);
		$cek = $this->db->affected_rows();

		return $msg = ($cek == 1) ? $this->session->set_flashdata('result','Data Berhasil Disimpan'):$this->session->set_flashdata('result','Data Gagal Disimpan'); 
	}

	

	public function update_data($table, $data, $where)
	{
		$q = $this->db->update($table, $data, $where);
		if(!$q)
		{
			return $this->session->set_flashdata('result','Data Gagal Diperbarui');
		}

		$cek = $this->db->affected_rows();

		return $msg = ($cek == 1) ? $this->session->set_flashdata('result','Data Berhasil Diperbarui'):$this->session->set_flashdata('result','Data berhasil diperbarui dengan data yang sama.'); 
	}

	public function ambilKode($id, $table, $kode)
	{
		$q = $this->db->query("select MAX(RIGHT($id,4)) as kd_max from $table");
		$kd = "";
		if($q->num_rows() > 0)
		{
			foreach ($q->result() as $k) 
			{
				$tmp = ((int)$k->kd_max)+1;
				$kd = sprintf("%05s", $tmp);
			}
		}
		else
		{
			$kd = 00001;
		}
		return $kode.$kd;
	}

	public function ambilDataById($table, $id)
	{
		$q = $this->db->get_where($table, $id);
		return $q->result();
	}

	public function set_banner($id)
	{
		$cek = $this->db->get_where('toa_banner', array('status'=>1));
		if($this->db->affected_rows()==1)
		{
			$q = $this->db->update('toa_banner', array('status'=>0) ,array('status'=>1));
			if($this->db->affected_rows() == 1)
			{
				$q2 = $this->db->update('toa_banner', array('status'=>1), array('kode_banner'=>$id));
				return $result = ($this->db->affected_rows()==1)? $this->session->set_flashdata('result','Tampilan utama banner berhasil diperbarui.'):$this->session->set_flashdata('result','Tampilan utama banner gagal diperbarui.'); 
			}
			else
			{
				return $this->session->set_flashdata('result','Tampilan utama banner gagal diperbarui.'); 
		
			}
		}

		$q3 = $this->db->update('toa_banner', array('status'=>1), array('kode_banner'=>$id));
		return $result = ($this->db->affected_rows()==1)? $this->session->set_flashdata('result','Tampilan utama banner berhasil diperbarui.'):$this->session->set_flashdata('result','Tampilan utama banner gagal diperbarui.'); 


	}

	public function ubahTanggal($tgl)
	{
		$this->load->helper('array');
		$bln = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
		$temp = explode('-', $tgl);
		$temp_bln = $temp[1];
		$real_bln = element($temp_bln, $bln);
		return $tgl_baru = $temp[2].' '.$real_bln.' '.$temp[0];
	}

	public function lihatPesanan($limit, $offset)
	{
		return $q = $this->db->query("SELECT a.*,b.email 
								FROM toa_order a
								LEFT JOIN toa_member b
								ON a.kode_member = b.kode_member LIMIT $offset, $limit");

	}

	public function JumlahlihatPesanan()
	{
		$q = $this->db->query("SELECT a.*,b.email 
								FROM toa_order a
								LEFT JOIN toa_member b
								ON a.kode_member = b.kode_member");
		return $q->num_rows();

	}

	public function lihatPesananByStatus($id, $limit, $offset)
	{
		return $q = $this->db->query("SELECT a.*,b.email 
								FROM toa_order a
								LEFT JOIN toa_member b
								ON a.kode_member = b.kode_member
								WHERE a.status = '$id' LIMIT $offset, $limit ");

	}

	public function JumlahPesananByStatus($id)
	{
		$q = $this->db->query("SELECT a.*,b.email 
								FROM toa_order a
								LEFT JOIN toa_member b
								ON a.kode_member = b.kode_member
								WHERE a.status = '$id'");
		return $q->num_rows();

	}

	public function lihatKonfirmasi($id)
	{
		return $q = $this->db->query(" 	SELECT a.kode_order, a.alamat1, a.alamat2, a.sub_total, a.ongkir, a.total_bayar, b.nama_kurir, c.email
										FROM toa_order a
										JOIN toa_kurir b
										JOIN toa_member c
										WHERE a.kode_order = '$id'
										AND b.kode_kurir = a.kode_kurir
										AND c.kode_member = a.kode_member ");
	}

	public function hitungBeratBarang($id)
	{
		$berat = $this->db->query("SELECT SUM( sub_berat ) AS total_berat
									FROM toa_detail_order
									WHERE kode_order = '$id' ");
		
		$row = $berat->row_array();
		return $row['total_berat'];
	}
}
/* End of file AdminModel.php */
/* Location: ./application/models/AdminModel.php */