<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webmodel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function cekLogin($data)
	{
		$query = $this->db->query("SELECT password FROM toa_member WHERE email = '".$data['email']."' ");
		//cek apakah data ditemukan
		if($query->num_rows() == 1)
		{
			$row = $query->row_array();
			if (password_verify($data['password'], $row['password'])) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		}
		else
		{
			return FALSE;
		}
	}

	public function cekUserStatus($data)
	{
		$query = $this->db->query("SELECT password,kode_member,email FROM toa_member WHERE email = '".$data['email']."' AND status = '".$data['status']."' ");
		//cek apakah data ditemukan
		if($query->num_rows() == 1)
		{
			// jika data ditemukan maka buat session
			$row = $query->row_array();
			if (password_verify($data['password'], $row['password'])) {
				$sess_data = array("kode" => $row['kode_member'],"email" => $row['email'], "isLoggedIn" => "userTOA");
				$this->session->set_userdata($sess_data);
				return TRUE;
			} else {
				return FALSE;
			}
			
		}
		else
		{
			return FALSE;
		}

	}

	public function cekDuplikat($data)
	{
		$query = $this->db->get_where('toa_member', array('email'=>$data));
		//cek apakah data ditemukan
		if($query->num_rows() == 1)
		{
			
			return FALSE;
		}
		else
		{
			return TRUE;
		}

	}

	public function regUser($data)
	{
		$query = $this->db->insert('toa_member', $data);
		//cek apakah data ditemukan
		return $result = ($this->db->affected_rows() == 1) ? TRUE:FALSE;

	}

	public function ambilKode($id, $table, $kode)
	{
		$q = $this->db->query("select MAX(RIGHT($id,4)) as kd_max from $table");
		$kd = "";
		if($q->num_rows() > 0)
		{
			foreach ($q->result() as $k) 
			{
				$tmp = ((int)$k->kd_max)+1;
				$kd = sprintf("%05s", $tmp);
			}
		}
		else
		{
			$kd = 00001;
		}
		return $kode.$kd;
	}

	public function cekUserKey($kode)
	{
		$q = $this->db->get_where('toa_member', array('key'=>$kode));
		return $result = ($q->num_rows() == 1) ? TRUE:FALSE;
	}

	public function activateKey($kode)
	{
		$this->db->update('toa_member', array('key'=>'', 'status'=>1), array('key'=>$kode));
		return $result = ($this->db->affected_rows() == 1) ? TRUE:FALSE;
	}

	public function resetPW($data)
	{
		$acak = rand(11,99).date("dmY");
		$temp = md5($acak."_toaUser");
		$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
		$options = [
		'cost' => 12,
		'salt' => $salt,
		];
		$newPass = password_hash($temp, PASSWORD_BCRYPT, $options);
		$this->db->update('toa_member', array('password'=>$newPass), $data);
		return $result = ($this->db->affected_rows() == 1) ? $temp:FALSE;
	}

	public function lihatLokasi()
	{
		return $q = $this->db->query("SELECT * FROM toa_lokasi where lokasi_kabupatenkota=0 and lokasi_kecamatan=0 and lokasi_kelurahan=0 order by lokasi_nama");
	}

	public function lihatKota($id)
	{
		return $q = $this->db->query("SELECT * FROM toa_lokasi where lokasi_propinsi=$id and lokasi_kecamatan=0 and lokasi_kelurahan=0 and lokasi_kabupatenkota!=0 order by lokasi_nama");
	}

	public function lihatKecamatan($id, $id2)
	{
		return $q = $this->db->query("SELECT * FROM toa_lokasi where lokasi_propinsi=$id and lokasi_kecamatan!=0 and lokasi_kelurahan=0 and lokasi_kabupatenkota=$id2 order by lokasi_nama");
	}

	public function lihatKelurahan($id, $id2, $id3)
	{
		return $q = $this->db->query("SELECT * FROM toa_lokasi where lokasi_propinsi=$id and lokasi_kecamatan=$id3 and lokasi_kelurahan!=0 and lokasi_kabupatenkota=$id2 order by lokasi_nama");
	}

	public function simpanOrder($data)
	{
		$this->db->insert('toa_order', $data);

		return $result = ($this->db->affected_rows()==1) ? TRUE:FALSE;
	}

	public function simpan_detailOrder($data)
	{
		$this->db->insert('toa_detail_order', $data);

		return $result = ($this->db->affected_rows()==1) ? TRUE:FALSE;
	}

	public function cekPassLama($pass_lama, $kode)
	{
		$query = $this->db->query("SELECT password FROM toa_member WHERE kode_member = '".$kode."' ");
		//cek apakah data ditemukan
		if($query->num_rows() == 1)
		{
			$row = $query->row_array();
			if (password_verify($pass_lama, $row['password'])) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
		
	}

	public function update_data($table, $data, $where)
	{
		$q = $this->db->update($table, $data, $where);
		if(!$q)
		{
			return $this->session->set_flashdata('result','Data Gagal Diperbarui');
		}

		$cek = $this->db->affected_rows();

		return $msg = ($cek == 1) ? $this->session->set_flashdata('result','Data Berhasil Diperbarui'):$this->session->set_flashdata('result','Data berhasil diperbarui dengan data yang sama.'); 
	}

	
	public function simpan_data($table, $data)
	{
		$q = $this->db->insert($table, $data);
		$cek = $this->db->affected_rows();

		return $msg = ($cek == 1) ? $this->session->set_flashdata('result','Data Berhasil Disimpan'):$this->session->set_flashdata('result','Data Gagal Disimpan'); 
	}

	public function simpan_testimoni($table, $data)
	{
		$q = $this->db->insert($table, $data);
		$cek = $this->db->affected_rows();

		return $msg = ($cek == 1) ? TRUE : FALSE ; 
	}

	public function kirim_data_konfirmasi($table, $data)
	{
		$q = $this->db->insert($table, $data);
		$cek = $this->db->affected_rows();

		return $msg = ($cek == 1) ? $this->session->set_flashdata('resultkonf','Konfirmasi pembayaran telah berhasil dilakukan. <br>Setelah kami memverifikasi data anda, kami akan mengirimkan email pemberitahuan yang berisi resi pengiriman pesanan anda.<br> Anda akan dialihkan ke halaman member..'):$this->session->set_flashdata('result','Data gagal dikirim, silakan coba lagi..'); 
	}

	public function update_alamat($data)
	{
		$q = $this->db->update('toa_order', $data, array('kode_order'=>$data['kode_order']));
	

		return $cek = ($this->db->affected_rows() == 1) ? true:false;
	}

	function HitungBaca($id)
	{
		$q = $this->db->query("update toa_artikel set dibaca=dibaca+1 where kode_artikel='$id'");
	}

	function get_feeds()
    {
     	$query = $this->db->get('toa_artikel');
		return $query->result();
    }
	

}

/* End of file WebModel.php */
/* Location: ./application/models/WebModel.php */