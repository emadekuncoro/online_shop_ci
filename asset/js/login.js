$(document).ready(function() {
	$('.panel').hide();
	$('.panel').fadeIn(1000);
	$("#login form").submit(function() {
			// menampung data
			data = $("#login form").serialize();
			$("#login button").html('Memproses...');
			$.ajax({
				url: $(this).prop('action') ,
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(msg) {
					if(msg.success==true) { //jika login berhasil maka muncul pesan sukses
						$(".form-group").hide(200);
						$('.login-message').addClass('alert alert-info text-center').html(msg.isi);
						setTimeout(function(){ window.location.reload() },1200);
					}
					else
					{
						//jika login gagal maka muncul pesan error
						$(".form-group").hide(200);
						$('.login-message').addClass('alert alert-warning text-center').html(msg.isi);
						setTimeout(function(){ window.location.reload() },1200);
					}
				}
			});
		});
});